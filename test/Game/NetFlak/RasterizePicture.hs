{-# LANGUAGE FlexibleContexts #-}

module Game.NetFlak.RasterizePicture
  ( Raster
  , Cell
  , rasterizePicture
  ) where

import Control.Monad.State

import Data.Array
import Data.List (concat)
import Data.Text.Lazy (unpack)
import Data.Vector (toList)

import Graphics.Vty
import Graphics.Vty.PictureToSpans
import Graphics.Vty.Span

type Index = Int

type Raster = Array (Index, Index)

type Cell = (Maybe Char, FixedAttr)

rasterizePicture :: Picture -> DisplayRegion -> Raster Cell
rasterizePicture pic screenSize@(srs, scs) =
  listArray ((0, 0), (srs - 1, scs - 1)) $
    concat $ 
      flip evalState defFixedAttr $
        mapM spanOpToCells $
          concatMap toList $
            displayOpsForPic pic screenSize

defFixedAttr :: FixedAttr
defFixedAttr = FixedAttr
  { fixedStyle     = defaultStyleMask
  , fixedForeColor = Nothing
  , fixedBackColor = Nothing
  , fixedURL       = Nothing
  }

maybeDefault :: a -> a -> (v -> a) -> MaybeDefault v -> a
maybeDefault def _    _ Default     = def
maybeDefault _   curr _ KeepCurrent = curr
maybeDefault _   _    f (SetTo new) = f new

applyAttr :: Attr -> FixedAttr -> FixedAttr
applyAttr attr fixedAttr = FixedAttr
  { fixedStyle     = maybeDefault defaultStyleMask (fixedStyle fixedAttr) id $ attrStyle attr
  , fixedForeColor = maybeDefault Nothing (fixedForeColor fixedAttr) Just $ attrForeColor attr
  , fixedBackColor = maybeDefault Nothing (fixedBackColor fixedAttr) Just $ attrBackColor attr
  , fixedURL       = maybeDefault Nothing (fixedURL fixedAttr) Just $ attrURL attr
  }

applyAttrs :: MonadState FixedAttr m => [Maybe Char] -> m [Cell]
applyAttrs chars = gets $ (<$> chars) . flip (,)

spanOpToCells :: MonadState FixedAttr m => SpanOp -> m [Cell]
spanOpToCells (TextSpan spanAttr outputWidth charWidth text)
  | charWidth /= outputWidth = error "unimplemented for character of width not equal to one"
  | charWidth == outputWidth = modify (applyAttr spanAttr) >> applyAttrs (Just <$> unpack text)
spanOpToCells (Skip num)     = applyAttrs $ replicate num Nothing
spanOpToCells (RowEnd num)   = applyAttrs $ replicate num Nothing
