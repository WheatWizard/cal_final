module Game.NetFlak.LogsSpec (spec) where

import Test.Hspec
import Test.QuickCheck

import Game.NetFlak.ArbitraryInstances.LogItem
import Game.NetFlak.ArbitraryInstances.EnvironmentVariables
import Game.NetFlak.Logs
import Game.NetFlak.Gen
import Game.NetFlak.RichString
import Game.NetFlak.TestHelpers

import Game.NetFlak.Types.LogItem
import Game.NetFlak.Types.RichString

-- Could be faster but I'm lazy right now
endsWith :: String -> String -> Bool
endsWith a b
 | a == b = True
endsWith (a : b) c = endsWith b c 
endsWith [] a = False

getLast :: [a] -> Maybe a
getLast [] = Nothing
getLast [a] = Just a
getLast (a : b : c) = getLast $ b : c

spec :: Spec
spec = do
  describe "fillLog" $ do
    it "always puts something in the log" $ property $
      \ log ->
        forAll (fillLog log) $
          (`shouldNotBe` [])
    it "never ends a rich log with \" .\"" $ property $
      \ log ->
        forAll (fillLog log) $
          mapM_ $ (`shouldNotSatisfy` (`endsWith` " .")) . rawContent
    it "never ends a rich log with \" !\"" $ property $
      \ log ->
        forAll (fillLog log) $
          mapM_ $ (`shouldNotSatisfy` (`endsWith` " !")) . rawContent
    it "never ends a rich log with \" ?\"" $ property $
      \ log ->
        forAll (fillLog log) $
          mapM_ $ (`shouldNotSatisfy` (`endsWith` " ?")) . rawContent
    it "always ends rich logs other than area banners with punctuation" $
      forAll (arbitrary `suchThat` (not . isBanner)) $
        \ log ->
          forAll (fillLog log) $
            mapM_ $ (shouldContain $ Just <$> "?!.") . (: []) . getLast . rawContent
    context "when passed a area banner" $
      it "doesn't end the rich log with punctuation" $ property $
        \ area ->
          forAll (fillLog $ AreaBanner area) $
            \ [ filledLog ] ->
              Just <$> "?!." `shouldNotContain` [getLast (rawContent filledLog)]
