module Game.NetFlak.BrainflakSpec (spec) where

import Game.NetFlak.Brainflak

import Test.Hspec
import Test.QuickCheck

testIncr :: Integer -> Bool
testIncr n = Just (n + 1) == brainflakValueFrom [n] "({}())" 6

testAdd :: (Integer, Integer) -> Bool
testAdd (n, m) = Just (n + m) == brainflakValueFrom [n, m] "({}{})" 6

spec :: Spec
spec = do
  describe "brainflakValueFrom" $ do
    it "adds 1 to a number with ({}())" $ property $
      testIncr
    it "adds the top two numbers with ({}{})" $ property $
      testAdd
    it "squares a positive number with {({})({}[()])}" $
      forAll ((arbitrary :: Gen Integer) `suchThat` (>0)) $
        \n ->
          Just (n ^ 2) == brainflakValueFrom [n] "{({})({}[()])}" (10*n + 3)
