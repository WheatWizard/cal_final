{- Language : NoMonomorphismRestriction -}

module Game.NetFlak.TestHelpers where

import Control.Monad (liftM2, join)

import Data.Map (empty, toList)

import Lens.Simple (view)

import Test.QuickCheck

import Test.Hspec

import Game.NetFlak.Types.LogItem
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Spell
import Game.NetFlak.Items

shouldAllSatisfy :: (HasCallStack, Show a) => [a] -> (a -> Bool) -> Expectation
v `shouldAllSatisfy` p = case failures of
  (a : b) -> expectationFailure $ "predicate failed on " ++ show a
  []      -> return ()
  where
    failures = filter (not . p) v

noTwoSatisfyFrom :: (HasCallStack, Show a) => (a -> a -> Bool) -> [a] -> Expectation
p `noTwoSatisfyFrom` v = case failures of
  ((a, b) : _) -> expectationFailure $ "predicate passed on " ++ show a ++ " and " ++ show b
  []           -> return ()
  where
    failures =
      [ (a, b)
      | (a, i) <- zip v [0 ..]
      , (b, j) <- zip v [0 ..]
      , i /= j
      , p a b
      ]

isBanner :: LogItem -> Bool
isBanner (AreaBanner _) = True
isBanner _ = False

-- Move elsewhere?
isLimited :: SpellComponent -> Bool
isLimited (LimitedSC _ _) = True
isLimited _ = False

singleUse :: SpellComponent -> Bool
singleUse (LimitedSC 1 _) = True
singleUse _ = False

-- Like listOf but with an expected size of 2
-- Useful because some test cases can take too long with listOf
smallListOf :: Gen a -> Gen [ a ]
smallListOf a = join $ elements [
    return []
  , liftM2 (:) a (smallListOf a)
  ]

componentContents :: SpellComponent -> ComponentStructure
componentContents (LimitedSC _ x) = x
componentContents (UnlimitedSC x) = x

spellStrings :: Gen String
spellStrings = listOf $ elements "()[]{}<>XY"

smallSpellStrings :: Gen String
smallSpellStrings = smallListOf $ elements "()[]{}XY"

balancedStrings :: Gen String
balancedStrings = do
  join $ elements
    [ return ""
    , return ""
    -- We use a join to make the expected halting time finite
    , join $ elements [
        liftM2 (\a b -> '(' : a ++ ')' : b) balancedStrings balancedStrings
      , liftM2 (\a b -> '{' : a ++ '}' : b) balancedStrings balancedStrings
      , liftM2 (\a b -> '<' : a ++ '>' : b) balancedStrings balancedStrings
      , liftM2 (\a b -> '[' : a ++ ']' : b) balancedStrings balancedStrings
      ]
    ]

getUnusableComponent :: Spell -> Gen SpellComponent
getUnusableComponent spell = do
   structure <- elements $ fst <$> toList (view usedComps spell)
   return $ LimitedSC 1 structure

spellIsNull :: Spell -> Bool
spellIsNull spell = empty == view usedComps spell 

