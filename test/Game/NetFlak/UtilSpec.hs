module Game.NetFlak.UtilSpec (spec) where

import Data.List (sort, genericLength)

import Test.Hspec
import Test.QuickCheck

import Game.NetFlak.Util
import Game.NetFlak.TestHelpers

spec :: Spec
spec = do
  describe "unbalanced" $ do
    it "never finds unbalanced braces in a balanced string" $
      forAll balancedStrings $
        \string ->
           unbalanced string `shouldBe` ([], [], [])
    it "unbalanced finds are sorted" $
      forAll spellStrings $
        \string ->
          let (a, b, c) = unbalanced string in
            (a, b, c) `shouldBe` (sort a, sort b, sort c)
    it "unopened indices are all in the string" $
      forAll spellStrings $
        \string ->
          let (a, b, c) = unbalanced string in
            let withinBound x = 0 <= x && x < genericLength string in
              c `shouldAllSatisfy` withinBound
    it "unclosed indices are all in the string" $
      forAll spellStrings $
        \string ->
          let (a, b, c) = unbalanced string in
            let withinBound x = 0 <= x && x < genericLength string in
              a `shouldAllSatisfy` withinBound
    it "mismatched indices are all in the string" $
      forAll spellStrings $
        \string ->
          let (a, b, c) = unbalanced string in
            let withinBound x = 0 <= x && x < genericLength string in
              b `shouldAllSatisfy` withinBound
    it "always finds an even number of mismatches" $
      forAll spellStrings $
        \string ->
           let (_, mismatches, _) = unbalanced string in
             even $ length mismatches
    it "only finds unclosed braces after the last unopened brace" $
      forAll spellStrings $
        \string ->
           let (unclosed, _, unopened) = unbalanced string in
             and $ do
               a <- unopened
               b <- unclosed
               return $ a < b
    it "finds unopened and unclosed braces between an even number of mismatches" $
      forAll spellStrings $
        \string ->
          let (unclosed, mismatches, unopened) = unbalanced string in
            [ filter (> a) mismatches | a <- unclosed ++ unopened ] `shouldAllSatisfy` (even . length)
    it "never puts a brace in two lists" $
      forAll spellStrings $
        \string ->
          let (unclosed, mismatches, unopened) = unbalanced string in
            and [ notElem a mismatches | a <- unclosed ] &&
              and [ notElem a unclosed | a <- unopened ] &&
                and [ notElem a unopened | a <- mismatches ]
    it "returns a number of errors with the same parity as the number of braces in the input" $
      forAll (listOf $ elements "()[]<>{}") $
        \string ->
          let (unclosed, mismatches, unopened) = unbalanced string in
            mod (length string) 2 `shouldBe` mod (length unclosed + length mismatches + length unopened) 2
    it "only finds braces to be unbalanced" $
      forAll spellStrings $
        \string -> 
          let (unclosed, mismatches, unopened) = unbalanced string in
            (unclosed ++ mismatches ++ unopened) `shouldAllSatisfy` (flip elem "()[]<>{}" . (string !!))

  describe "mirror" $ do
    context "when given a balanced string" $
      it "returns a balanced string" $
        forAll balancedStrings $
          \string ->
            unbalanced (mirror string) `shouldBe` ([], [], [])
    context "when given an unbalanced string" $
      it "returns an unbalanced string" $
        forAll (spellStrings `suchThat` (not . isBalanced)) $
          \string ->
            unbalanced (mirror string) `shouldNotBe` ([], [], [])
    it "returns a string with the same length as the input" $
      forAll spellStrings $
        \string ->
          length (mirror string) `shouldBe` length string
            
  describe "trim" $ do
    context "when the desired length is negative" $
      it "returns an empty string" $
        forAll (arbitrary `suchThat` (< 0)) $
          \len ->
            \str ->
               null (trim (len :: Integer) str)
    context "when the desired length is zero" $
      it "returns an empty string" $ property $
        \str -> null (trim 0 str)
    context "when the desired length is one and the string has length greater than one" $
      it "returns \".\"" $ property $
        forAll (arbitrary `suchThat` ((> 1) . length)) $
          \str -> trim 1 str `shouldBe` "."
    context "when the desired length is two and the string has length greater than two" $
      it "returns \"..\"" $ property $
        forAll (arbitrary `suchThat` ((> 2) . length)) $
          \str -> trim 2 str `shouldBe` ".."
    context "when the desired length is greater than or equal to the string length" $
      it "returns the input string" $ property $
        \str ->
          forAll (arbitrary `suchThat` (>= length str)) $
            \len ->
              trim len str `shouldBe` str
    context "when the desired length is non-negative" $
      it "returns a string less than or equal to the desired length" $
        forAll (arbitrary `suchThat` (>= 0)) $
          \len ->
            \str ->
              length (trim len str) <= len
  describe "charToNum" $ do
    context "when called on 0" $
      it "returns 0" $
        charToNum '0' `shouldBe` 0
    context "when called on 1" $
      it "returns 1" $
        charToNum '1' `shouldBe` 1
    context "when called on 2" $
      it "returns 2" $
        charToNum '2' `shouldBe` 2
    context "when called on 3" $
      it "returns 3" $
        charToNum '3' `shouldBe` 3
    context "when called on 4" $
      it "returns 4" $
        charToNum '4' `shouldBe` 4
    context "when called on 5" $
      it "returns 5" $
        charToNum '5' `shouldBe` 5
    context "when called on 6" $
      it "returns 6" $
        charToNum '6' `shouldBe` 6
    context "when called on 7" $
      it "returns 7" $
        charToNum '7' `shouldBe` 7
    context "when called on 8" $
      it "returns 8" $
        charToNum '8' `shouldBe` 8
    context "when called on 9" $
      it "returns 9" $
        charToNum '9' `shouldBe` 9
