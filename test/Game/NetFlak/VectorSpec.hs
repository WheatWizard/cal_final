module Game.NetFlak.VectorSpec (spec) where

import Test.Hspec
import Test.QuickCheck

import Game.NetFlak.Types.Vector
import Game.NetFlak.TestHelpers

ascTest :: (Vector -> Vector -> Vector) -> Vector -> Vector -> Vector -> Bool
ascTest (+) a b c = (a + b) + c == a + (b + c)

comTest :: Vector -> Vector -> Bool
comTest a b = a + b == b + a

idTest :: (Vector -> Vector -> Vector) -> Vector -> Vector -> Bool
idTest (+) a b = a + b == b && b + a == b

invTest :: Vector -> Bool
invTest a = a + negate a == fromInteger 0

distTest :: Vector -> Vector -> Vector -> Bool
distTest a b c = a * (b + c) == (a * b) + (a * c) && (b + c) * a == (b * a) + (c * a)

absTest :: Vector -> Bool
absTest x = abs x * signum x == x

spec :: Spec
spec = do
  describe "manhattan" $ do
    it "obeys the identity of indiscernables" $ property $
      \vec -> manhattan vec vec == 0
    it "obeys symmetry" $ property $
      \vec1 vec2 -> manhattan vec1 vec2 == manhattan vec2 vec1
    it "obeys the triangle inequality" $ property $
      \vec1 vec2 vec3 -> manhattan vec1 vec3 <= manhattan vec1 vec2 + manhattan vec2 vec3
    it "obeys non-negativity" $ property $ -- Technically redundant at this point
      \vec1 vec2 -> manhattan vec1 vec2 >= 0
  describe "Num Vector" $ do
    it "is associative on (+)" $ property $ ascTest (+)
    it "is commutative on (+)" $ property comTest
    it "has an identity on (+) of fromInteger 0" $ property $ idTest (+) $ fromInteger 0
    it "has inverses with negate" $ property invTest
    it "is associative on (*)" $ property $ ascTest (*)
    it "has an identity on (*) of fromInteger 1" $ property $ idTest (*) $ fromInteger 1
    it "is distributive" $ property distTest
    it "follows the abs signum law" $ property absTest
