module Game.NetFlak.ArbitraryInstances.Inventory where

import Data.Map (empty)

import Game.NetFlak.ArbitraryInstances.SpellComponent
import Game.NetFlak.ArbitraryInstances.Potion
import Game.NetFlak.Types.Inventory
import Game.NetFlak.World

import Test.QuickCheck

instance Arbitrary Inventory where
  arbitrary = do
    heldComponents <- listOf arbitrary
    heldPotions   <- listOf arbitrary
    quickPotion    <- arbitrary
    let
      emptyInventory = Inventory 
        { _heldComponents = []
        , _heldPotions   = empty
        , _quickPotion    = quickPotion
        , _hotbarMap      = empty
        }
    -- Build the inventory by picking up the pieces as if by play rather than inserting them
    return $ pickUp (heldComponents, heldPotions) emptyInventory
