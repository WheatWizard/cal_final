module Game.NetFlak.ArbitraryInstances.LogItem where

import Game.NetFlak.Types.LogItem

import Game.NetFlak.ArbitraryInstances.Enemy
import Game.NetFlak.ArbitraryInstances.EnvironmentVariables
import Game.NetFlak.ArbitraryInstances.Spell
import Game.NetFlak.ArbitraryInstances.Potion
import Game.NetFlak.ArbitraryInstances.CauseOfDeath

import Test.QuickCheck

import Control.Monad

-- TODO PickUp, CannotUse
instance Arbitrary LogItem where
  arbitrary =
    oneof
      [ liftM3 EnemyAttack arbitrary arbitrary arbitrary
      , liftM2 EnemyDeath  arbitrary arbitrary
      , AreaBanner    <$> arbitrary
      , SpellCast     <$> arbitrary
      , UsePotion     <$> arbitrary
      , BadPotion     <$> arbitrary
      , StatueAwakens <$> arbitrary
      , return UnbalancedBraces
      , return TimeOut
      , return DoorOpens
      , return PlayerDies
      ]
