module Game.NetFlak.ArbitraryInstances.Tools (genericGetSize) where

import Test.QuickCheck

genericGetSize :: (Integral a) => Gen a
genericGetSize = (fromInteger . toInteger) <$> getSize 

