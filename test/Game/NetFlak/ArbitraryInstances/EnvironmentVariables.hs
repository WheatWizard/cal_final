module Game.NetFlak.ArbitraryInstances.EnvironmentVariables where

import Game.NetFlak.ArbitraryInstances.Tools
import Game.NetFlak.ArbitraryInstances.SpecialArea

import Game.NetFlak.Types.EnvironmentVariables

import Test.QuickCheck


instance Arbitrary EnvironmentVariables where
  arbitrary = do
    n <- genericGetSize `suchThat` (>0)

    area <- arbitrary

    diff <- choose (0, n)

    return $ EnvironmentVariables
      { _difficulty = diff
      , _specialArea = area
      }
