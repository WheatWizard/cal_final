module Game.NetFlak.ArbitraryInstances.Door where

import Control.Monad (liftM5)

import Game.NetFlak.Types.Door

import Test.QuickCheck

instance Arbitrary DoorCondition where
  arbitrary = elements
    [ Always
    , LevelClear
    , Never
    ]

instance Arbitrary a => Arbitrary (Door a) where
  arbitrary = liftM5 Door arbitrary arbitrary arbitrary arbitrary arbitrary
    
