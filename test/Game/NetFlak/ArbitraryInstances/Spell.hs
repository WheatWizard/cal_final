module Game.NetFlak.ArbitraryInstances.Spell where

import Data.Map (empty)

import Game.NetFlak.ArbitraryInstances.SpellComponent
import Game.NetFlak.Types.Spell
import Game.NetFlak.Spell

import Test.QuickCheck

instance Arbitrary Spell where
  arbitrary =
     foldr addComponent emptySpell <$> listOf arbitrary
