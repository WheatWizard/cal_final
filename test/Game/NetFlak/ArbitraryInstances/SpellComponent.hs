module Game.NetFlak.ArbitraryInstances.SpellComponent where

import Control.Monad

import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.ArbitraryInstances.Tools
import Game.NetFlak.TestHelpers

import Test.QuickCheck

instance Arbitrary SpellComponent where
  arbitrary = do
    -- Override the default implementation as it tends to take too long
    -- structure <- listOf spellStrings

    structure <- smallListOf spellStrings

    n <- genericGetSize `suchThat` (>0)

    uses <- choose (1, n)

    elements [UnlimitedSC structure, LimitedSC uses structure]

