module Game.NetFlak.ArbitraryInstances.Enemy where

import Game.NetFlak.ArbitraryInstances.Tools
import Game.NetFlak.ArbitraryInstances.Potion
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyMemory
import Game.NetFlak.Types.PseudoColor

import Test.QuickCheck

instance Arbitrary Enemy where
  arbitrary = do
    n <- genericGetSize `suchThat` (>0)

    health <- choose (-n, n)

    damage <- choose (0, n)

    stack <- listOf $ choose (-n, n)

    arbString <- arbitrary :: Gen String

    arbIcon  <- arbitrary :: Gen Char

    ai <- elements [Simple, Inactive]

    attackEffects <- arbitrary

    statueHealth <- arbitrary
   
    return $ Enemy {
        _iconChar      = arbIcon
      -- TODO arbitrary Growth patterns
      , _enemyGrowth   = NoChange
      -- TODO arbitrary color
      , _iconPColor    = RedPID
      -- TODO arbitrary memory
      , _memories      = NoMovement
      , _name          = arbString
      , _enemyHealth   = health
      , _enemyDamage   = damage
      , _enemyStack    = stack
      , _ai            = ai
      , _attackEffects = attackEffects
      , _statueHealth  = statueHealth
      }
