module Game.NetFlak.ArbitraryInstances.Player where

import Game.NetFlak.ArbitraryInstances.Inventory
import Game.NetFlak.ArbitraryInstances.Tools
import Game.NetFlak.Types.Player

import Test.QuickCheck

instance Arbitrary Player where
  arbitrary = do
    n <- genericGetSize `suchThat` (> 0)

    playerInv   <- arbitrary
    maxHealth   <- choose (1, n)
    damageTaken <- choose (0, maxHealth)

    effectStack <- arbitrary `suchThat` (>= 0)
    return $ Player {
        _playerInv = playerInv
      , _damageTaken = damageTaken
      , _maxHealth = maxHealth
      -- TODO
      , _bloodOnFeet = []
      , _effectStack = effectStack
      }

