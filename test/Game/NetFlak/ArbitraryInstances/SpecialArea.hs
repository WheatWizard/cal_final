module Game.NetFlak.ArbitraryInstances.SpecialArea where

import Game.NetFlak.ArbitraryInstances.Tools
import Game.NetFlak.Types.SpecialArea

import Test.QuickCheck

instance Arbitrary SpecialArea where
  arbitrary = elements
    [ Normal
    , Alchemist
    , Statue
    ]
