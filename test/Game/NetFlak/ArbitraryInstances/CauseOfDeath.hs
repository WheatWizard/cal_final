module Game.NetFlak.ArbitraryInstances.CauseOfDeath where

import Game.NetFlak.Types.EnemyAction

import Test.QuickCheck

instance Arbitrary CauseOfDeath where
  arbitrary =
    elements
      [ PlayerKill
      , Trampled
      , InWall
      ]
