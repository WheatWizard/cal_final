module Game.NetFlak.ArbitraryInstances.World where

import Control.Monad

import Lens.Simple

import Game.NetFlak.Logs
import Game.NetFlak.Gen

import Game.NetFlak.ArbitraryInstances.Player
import Game.NetFlak.ArbitraryInstances.Level
import Game.NetFlak.ArbitraryInstances.LogItem
import Game.NetFlak.ArbitraryInstances.Spell
import Game.NetFlak.ArbitraryInstances.Settings

import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.World

import Test.QuickCheck

instance Arbitrary a => Arbitrary (World a) where
  arbitrary = do
    plr <- arbitrary
    lvl <- arbitrary 
    let
      inBoundCoords :: Gen Vector
      inBoundCoords = liftM2 (,) (choose (0, fst $ view levelSize lvl)) (choose (0, snd $ view levelSize lvl))
    spl <- arbitrary
    stg <- arbitrary
    ded <- listOf $ liftM2 (,) inBoundCoords arbitrary
    raw <- listOf arbitrary
    rix <- listOf $ arbitrary >>= fillLog
    return $ World {
        _player       = plr
      , _currentLevel = lvl
      , _currentSpell = spl
      , _userSettings = stg
      , _deadEnemies  = ded
      -- TODO
      , _bloodTracks = []
      , _rawLog  = raw
      , _richLog = concat rix
      }

