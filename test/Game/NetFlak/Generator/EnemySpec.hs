module Game.NetFlak.Generator.EnemySpec (spec) where

import Game.NetFlak.ArbitraryInstances.EnvironmentVariables

import Game.NetFlak.Generator.Enemy

import Game.NetFlak.Gen

import Game.NetFlak.Types.Enemy

import Lens.Simple

import Test.Hspec
import Test.QuickCheck

import Control.Monad.Trans.Reader (runReaderT)

spec :: Spec
spec = do
  describe "generateEnemy" $
    it "never names an enemy the empty string" $
      forAll (arbitrary >>= runReaderT generateEnemy) $
        \ enemy ->
          view name enemy`shouldNotBe` ""
