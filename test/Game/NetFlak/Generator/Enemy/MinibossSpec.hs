module Game.NetFlak.Generator.Enemy.MinibossSpec (spec) where

import Game.NetFlak.ArbitraryInstances.EnvironmentVariables

import Game.NetFlak.Generator.Enemy.Miniboss

import Game.NetFlak.Gen

import Game.NetFlak.Types.Enemy

import Lens.Simple

import Test.Hspec
import Test.QuickCheck

import Control.Monad.Trans.Reader (runReaderT)

spec :: Spec
spec = do
  describe "generateMiniboss" $
    it "never names an enemy the empty string" $
      forAll (arbitrary >>= runReaderT generateMiniboss) $
        \ enemy ->
          view name enemy`shouldNotBe` ""
