module Game.NetFlak.Generator.Enemy.HallwaySpec (spec) where

import Game.NetFlak.ArbitraryInstances.EnvironmentVariables

import Game.NetFlak.Generator.Enemy.Hallway

import Game.NetFlak.Gen

import Game.NetFlak.Types.Enemy

import Lens.Simple

import Test.Hspec
import Test.QuickCheck

import Data.Set

import Control.Monad.Trans.Reader (runReaderT)
import Control.Monad.Trans.State  (runStateT, evalStateT)

spec :: Spec
spec = do
  describe "generateHallwayEnemy" $ do
    it "never names an enemy the empty string" $
      forAll (do
        bound <- arbitrary
        occupiedHealthes <- arbitrary
        arbitrary >>= runReaderT (evalStateT (generateHallwayEnemy bound) occupiedHealthes)) $
        \ enemy ->
          view name enemy `shouldNotBe` ""
    it "adds the enemy's health to the occupied healthes" $
      forAll (do
        bound <- arbitrary
        occupiedHealthes <- arbitrary
        arbitrary >>= runReaderT (runStateT (generateHallwayEnemy bound) occupiedHealthes)) $
        \ (enemy, occupiedHealthes) ->
          occupiedHealthes `shouldSatisfy` member (view enemyHealth enemy)

