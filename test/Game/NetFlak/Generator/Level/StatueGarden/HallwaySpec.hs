module Game.NetFlak.Generator.Level.StatueGarden.HallwaySpec (spec) where

import Game.NetFlak.Generator.Level.StatueGarden.Hallway

import Game.NetFlak.Generator.Helper (specFor)

import Game.NetFlak.ArbitraryInstances.EnvironmentVariables

import Game.NetFlak.Gen

import Test.Hspec
import Test.QuickCheck

import Control.Monad.Trans.Reader (runReaderT)

spec :: Spec
spec = describe "generateStatueGardenHallway" $ specFor $ arbitrary >>= runReaderT generateStatueGardenHallway
