module Game.NetFlak.Generator.Level.StatueGarden.TrophyRoomSpec (spec) where

import Game.NetFlak.Generator.Level.StatueGarden.TrophyRoom

import Game.NetFlak.Generator.Helper (specFor)

import Game.NetFlak.ArbitraryInstances.EnvironmentVariables

import Game.NetFlak.Gen

import Test.Hspec
import Test.QuickCheck

import Control.Monad.Trans.Reader (runReaderT)

spec :: Spec
spec = describe "generateTrophyRoom" $ specFor $ arbitrary >>= runReaderT generateTrophyRoom
