module Game.NetFlak.Generator.Level.Lab.HallwaySpec (spec) where

import Game.NetFlak.Generator.Level.Lab.Hallway (generateLabHallway)

import Game.NetFlak.Generator.Helper (specFor)

import Game.NetFlak.ArbitraryInstances.EnvironmentVariables

import Game.NetFlak.Gen

import Test.Hspec
import Test.QuickCheck

import Control.Monad.Trans.Reader (runReaderT)

spec :: Spec
spec = describe "generateCage" $ specFor $ arbitrary >>= runReaderT generateLabHallway
