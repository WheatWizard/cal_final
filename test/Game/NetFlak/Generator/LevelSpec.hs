module Game.NetFlak.Generator.LevelSpec (spec, specFor) where

import Game.NetFlak.ArbitraryInstances.EnvironmentVariables

import Game.NetFlak.Generator.Level
import Game.NetFlak.Generator.Helper

import Control.Monad.Trans.Reader (runReaderT)

import Test.Hspec
import Test.QuickCheck

spec :: Spec
spec = do
  describe "generateLevel" $ specFor $ arbitrary >>= runReaderT generateLevel

