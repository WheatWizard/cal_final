module Game.NetFlak.Generator.Helper (specFor) where

import Game.NetFlak.Gen
import Game.NetFlak.Level (inBounds, reachable)
import Game.NetFlak.TestHelpers

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.LogItem
import Game.NetFlak.Types.Vector

import Control.Monad (guard)

import Lens.Simple

import Test.Hspec
import Test.QuickCheck

overlappingEnemies :: [(Vector, Enemy)] -> [(Vector, Enemy, Enemy)]
overlappingEnemies enemies = do
  index1 <- [0 .. length enemies - 2]
  index2 <- [index1 + 1, length enemies -1]
  let
    (l1, e1) = enemies !! index1
    (l2, e2) = enemies !! index2
  guard (l1 == l2)
  return (l1, e1, e2)

specFor ::
-- A more general type signature exists but I am to lazy to make it work
    Gen EndlessLevel -> SpecWith ()
specFor generator = do
    it "has an enemy present" $
      forAll generator $
        \ generatedLevel ->
          generatedLevel `shouldNotSatisfy` null . view enemies
    it "puts the player in bounds" $
      forAll generator $
        \ generatedLevel ->
          view playerLocation generatedLevel `shouldSatisfy` inBounds generatedLevel
    it "puts all the enemies in bounds" $
      forAll generator $
        \ generatedLevel ->
          view enemies generatedLevel `shouldAllSatisfy` (inBounds generatedLevel . fst)
    it "puts all the components in bounds" $
      forAll generator $
        \ generatedLevel ->
          view floorComponents generatedLevel `shouldAllSatisfy` (inBounds generatedLevel . fst)
    it "puts all the potions in bounds" $
      forAll generator $
        \ generatedLevel ->
          view floorPotions generatedLevel `shouldAllSatisfy` (inBounds generatedLevel . fst)
    it "puts all the doors somewhere reachable" $
      forAll generator $
        \ generatedLevel ->
          (view doorPosition <$> view exitDoors generatedLevel) `shouldAllSatisfy` reachable generatedLevel
    -- TODO make and use a shouldAllNotBe
    it "does not put the player in the same space as an enemy" $
      forAll generator $
        \ generatedLevel ->
          view enemies generatedLevel `shouldAllSatisfy` ((/= view playerLocation generatedLevel) . fst)
    it "does not put two enemies in the same space" $
      forAll generator $
        \ generatedLevel ->
          case overlappingEnemies (view enemies generatedLevel) of
            ((location, e1, e2) : _) -> expectationFailure $ "test failed because enemies " ++ view name e1 ++ " and " ++ view name e2 ++ " are both at " ++ show location
            [] -> return ()
    -- TODO use shouldAllNotBe
    it "does not put the player on a component" $
      forAll generator $
        \ generatedLevel ->
          view floorComponents generatedLevel `shouldAllSatisfy` ((/= view playerLocation generatedLevel) . fst)
    -- TODO use shouldAllNotBe
    it "does not put the player on a potion" $
      forAll generator $
        \ generatedLevel ->
          view floorPotions generatedLevel `shouldAllSatisfy` ((/= view playerLocation generatedLevel) . fst)
    it "does not put an enemy on a component" $
      forAll generator $
        \ generatedLevel ->
          view floorComponents generatedLevel `shouldAllSatisfy` ((`notElem` (fst <$> view enemies generatedLevel)) . fst)
    it "does not put an enemy on a potion" $
      forAll generator $
        \ generatedLevel ->
          view floorPotions generatedLevel `shouldAllSatisfy` ((`notElem` (fst <$> view enemies generatedLevel)) . fst)
    it "does not put two potions in the same place" $
      forAll generator $
        \ generatedLevel ->
          ((. fst) . (==) . fst) `noTwoSatisfyFrom` view floorPotions generatedLevel
    it "does not put two components in the same place" $
      forAll generator $
        \ generatedLevel ->
          ((. fst) . (==) . fst) `noTwoSatisfyFrom` view floorComponents generatedLevel
    it "does not put a potion and a component in the same place" $
      forAll generator $
        \ generatedLevel ->
          view floorComponents generatedLevel `shouldAllSatisfy` ((`notElem` (fst <$> view floorPotions generatedLevel)) . fst)
    it "is not unwinnable by virtue of door location" $
      forAll generator $
        \ generatedLevel ->
          case view exitDoors generatedLevel of
            [] -> expectationFailure "There are no doors to exit"
            doors
              | all (== Never)  $ view openCondition  <$> doors
                -> expectationFailure "None of the doors can open"
              | all (== Always) $ view closeCondition <$> doors
                -> expectationFailure "All of the doors immediately close"
              | otherwise
                -> return ()
{-
    it "has an area banner" $
      forAll generator $
        \ generatedLevel ->
          view rawLogs generatedLevel `shouldSatisfy` any isBanner
    it "does not have an generation mismatch" $
      forAll generator $
        \ generatedLevel ->
          sequence_ $ do
            log <- view rawLogs generatedLevel
            case log of
              GenerationMismatch a b -> expectationFailure $ "Expected to generate " ++ show a ++ ". instead generated " ++ show b ++ "."
              otherwise -> return ()
          
-}
