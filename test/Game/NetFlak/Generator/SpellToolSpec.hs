{-# Language ScopedTypeVariables #-}
module Game.NetFlak.Generator.SpellToolSpec (spec) where

import Game.NetFlak.Generator.SpellTool

import Test.Hspec
import Test.QuickCheck

spec :: Spec
spec = do
  describe "(+*+)" $ do
    it "is associative" $ property $
      \ (a :: [String]) ->
        \ (b :: [String]) ->
          \ (c :: [String]) ->
            (a +*+ b) +*+ c `shouldBe` a +*+ (b +*+ c)
    it "has a left identity" $
      forAll (arbitrary `suchThat` ((> 0) . length)) $
        \ (a :: [String]) ->
          [""] +*+ a `shouldBe` a
    it "has a right identity" $
      forAll (arbitrary `suchThat` ((> 0) . length)) $
        \ (a :: [String]) ->
          a +*+ [""] `shouldBe` a
    it "adds the number of gaps" $
      forAll (arbitrary `suchThat` ((> 0) . length)) $
        \ (a :: [String]) ->
          forAll (arbitrary `suchThat` ((> 0) . length)) $
            \ (b :: [String]) ->
              length (a +*+ b) - 1 `shouldBe` (length a - 1) + (length b - 1)
  describe "compose" $ do
    it "is associative" $ property $
      \ (a :: [String]) ->
        \ (b :: [String]) ->
          \ (c :: [String]) ->
            compose (compose a b) c `shouldBe` compose a (compose b c)
    it "has a left identity" $
      forAll (arbitrary `suchThat` ((> 0) . length)) $
        \ (a :: [String]) ->
          compose ["", ""] a `shouldBe` a
    it "has a right identity" $ property $
      \ (a :: [String]) ->
        compose a ["", ""] `shouldBe` a
    it "multiplies the number of gaps" $
      forAll (arbitrary `suchThat` ((> 0) . length)) $
        \ (a :: [String]) ->
          forAll (arbitrary `suchThat` ((> 0) . length)) $
            \ (b :: [String]) ->
              length (compose a b) - 1 `shouldBe` (length a - 1) * (length b - 1)
      

