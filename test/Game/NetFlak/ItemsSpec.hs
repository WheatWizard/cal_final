{-# Language ScopedTypeVariables #-}
module Game.NetFlak.ItemsSpec (spec) where

import Data.Map (empty, toList)
import Data.Maybe (isNothing)
import Data.Char (toUpper)

import Test.Hspec
import Test.QuickCheck

import Game.NetFlak.ArbitraryInstances.Spell
import Game.NetFlak.ArbitraryInstances.World
import Game.NetFlak.Items
import Game.NetFlak.Spell
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.World
import Game.NetFlak.TestHelpers

capitalized :: String -> Bool
capitalized "" = True
capitalized (a:b) = a == toUpper a

doesNotClear :: SpellComponent -> Bool
doesNotClear (UnlimitedSC [])   = False
doesNotClear (UnlimitedSC [""]) = False
doesNotClear (UnlimitedSC a)    = True
doesNotClear (LimitedSC _ a) = doesNotClear $ UnlimitedSC a

spec :: Spec
spec = do
  describe "canUse" $ do
    it "always allows unlimited use components" $ property $
      \ structure ->
        \ (world :: World ()) ->
          -- Can't use shouldBe since LogItem is not a instance of Eq
          canUse (UnlimitedSC structure) world `shouldSatisfy` isNothing
  describe "amountUsed" $ do
    it "returns 0 on an empty spell" $ property $
      \structure ->
        amountUsed structure emptySpell `shouldBe` 0
    it "returns 1 if we use it once on an empty spell" $ property $
      \structure ->
        amountUsed structure (addComponent (LimitedSC 1 structure) emptySpell) `shouldBe` 1
    it "returns the same thing if we use a different component" $ property $
      \struct1 ->
        forAll (arbitrary `suchThat` (/= struct1)) $
          \struct2 ->
            \baseSpell ->
              amountUsed struct1 baseSpell `shouldBe` amountUsed struct1 (addComponent (LimitedSC 1 struct2) baseSpell)
  describe "itemDesc" $ do
    context "when describing use items" $ do
      it "is non-empty" $ property $
        \(item :: Potion) ->
          itemDesc item `shouldNotBe` ""
      it "should give different items different descriptions" $
        forAll (arbitrary :: Gen Potion) $
          \item1 ->
            forAll (arbitrary `suchThat` (/=item1)) $
              \item2 ->
                itemDesc item1 `shouldNotBe` itemDesc item2
      it "should end descriptions in a period" $ property $
        \(item :: Potion) ->
          last (itemDesc item) `shouldBe` '.'
      it "should capitalize descriptions" $ property $
        \(item :: Potion) ->
          itemDesc item `shouldSatisfy` capitalized
    context "when describing spell components" $ do
      it "is non-empty" $ property $
        \(item :: SpellComponent) ->
          itemDesc item `shouldNotBe` ""
      it "should end descriptions in a period" $ property $
        \(item :: SpellComponent) ->
          last (itemDesc item) `shouldBe` '.'
      it "should capitalize descriptions" $ property $
        \(item :: SpellComponent) ->
          itemDesc item `shouldSatisfy` capitalized
      context "and the spell component doesn't clear the spell" $
        it "should give different items different descriptions" $
          forAll (arbitrary `suchThat` doesNotClear) $
            \item1 ->
              forAll (arbitrary `suchThat` (/=item1)) $
                \item2 ->
                  itemDesc item1 `shouldNotBe` itemDesc item2
  describe "itemName" $ do
    context "when naming use items" $ do
      it "never chooses an empty string" $ property $
        \(item :: Potion) ->
          itemName item `shouldNotBe` ""
      it "never starts the name with a space" $ property $
        \(item :: Potion) ->
          head (itemName item) `shouldNotBe` ' '
      it "never names two different ones the same thing" $ property $
        \(item1 :: Potion) ->
          forAll (arbitrary `suchThat` (/= item1)) $
            \item2 ->
              itemName item1 `shouldNotBe` itemName item2
    context "when naming spell components" $ do
      it "never chooses an empty string" $ property $
        \(item :: SpellComponent) ->
          itemName item `shouldNotBe` ""
      -- This case is covered by the last one but it is not guarenteed to get caught
      -- and it has caused issues in the past #103
      it "never starts the name with a space" $ property $
        \(item :: SpellComponent) ->
          head (itemName item) `shouldNotBe` ' '
      it "never contains three or more \"/\"s in a row" $ property $
        \(item :: SpellComponent) ->
          itemName item `shouldNotContain` "///"
      it "names the limited and unlimited versions the same thing" $ property $
        \structure ->
          forAll (arbitrary `suchThat` (> 0)) $
            \n ->
              itemName (UnlimitedSC structure) `shouldBe` itemName (LimitedSC n structure)
      context "and the components are unlimited" $ do
        it "never names two different ones the same thing" $ property $
          forAll (smallListOf spellStrings `suchThat` (/= [])) $
            \structure1 ->
              forAll (smallListOf spellStrings `suchThat` (/= structure1) `suchThat` (not . null)) $
                \structure2 ->
                  itemName (UnlimitedSC structure1) `shouldNotBe` itemName (UnlimitedSC structure2)
        context "and the spell component just copies the input string a bunch of times" $ 
          it "never starts the name with a space" $ property $
            \n ->
              head (itemName (UnlimitedSC $ replicate n "")) `shouldNotBe` ' '
      context "and the components are limited" $ do
        it "never names two limited components with the same thing" $ property $
          forAll (arbitrary `suchThat` (/= [])) $
            \structure1 ->
              forAll (arbitrary `suchThat` (/= structure1) `suchThat` (not . null)) $
                \structure2 ->
                  forAll (arbitrary `suchThat` (> 0)) $
                    \n ->
                      itemName (LimitedSC n structure1) `shouldNotBe` itemName (LimitedSC n structure2)
        it "ignores the number of uses" $ property $
          \structure ->
            forAll (arbitrary `suchThat` (> 0)) $
              \n ->
                forAll (arbitrary `suchThat` (> 0)) $
                  \m ->
                    itemName (LimitedSC n structure) `shouldBe` itemName (LimitedSC m structure)
        context "and the spell component just copies the input string a bunch of times" $ 
          it "never starts the name with a space" $ property $
            forAll (arbitrary `suchThat` (> 0)) $
              \n ->
                forAll (arbitrary `suchThat` (>0)) $
                  \m -> 
                    head (itemName (LimitedSC m $ replicate n "")) `shouldNotBe` ' '
