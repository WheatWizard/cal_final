{-# Language ScopedTypeVariables #-}
module Game.NetFlak.WorldSpec (spec) where

import Lens.Simple

import Test.Hspec
import Test.QuickCheck

import Game.NetFlak.ArbitraryInstances.Inventory
import Game.NetFlak.ArbitraryInstances.Level
import Game.NetFlak.ArbitraryInstances.SpellComponent
import Game.NetFlak.ArbitraryInstances.Potion
import Game.NetFlak.ArbitraryInstances.World
import Game.NetFlak.Types.Inventory
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.LogItem
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.World
import Game.NetFlak.Util
import Game.NetFlak.World
import Game.NetFlak.TestHelpers

spec :: Spec
spec = do
  describe "logPotion" $ do
    it "changes the logs" $ property $
      \ (world :: World ()) ->
        \ potion ->
          view rawLog (logPotion potion world) `shouldNotBe` view rawLog world
    it "doesn't change things other than the logs" $ property $
      \ (world :: World ()) ->
        \ potion ->
          set rawLog (view rawLog world) (logPotion potion world) `shouldBe` world
  describe "applyPotion" $ do
    it "implements all the potions" $ property $
      \ (world :: World ()) ->
        \potion ->
          view rawLog (applyPotion potion world) `shouldNotBe` (BadPotion potion) : view rawLog world
    -- This test is stronger than the last test
    -- The last test is included because it directs us to the problem better than this test
    it "doesn't change the logs" $ property $
      \ (world :: World ()) ->
        \ potion ->
          view rawLog (applyPotion potion world) `shouldBe` view rawLog world
    context "when using mirror potion" $
      it "mirrors the current spell contents" $ property $
      \ (world :: World ()) ->
          view (currentSpell . spellContents) (applyPotion Mirror world) `shouldBe`
            mirror (view (currentSpell . spellContents) world)
    context "when using noitop" $
      it "reverses the current spell contents" $ property $
      \ (world :: World ()) ->
          view (currentSpell . spellContents) (applyPotion Noitop world) `shouldBe`
            reverse (view (currentSpell . spellContents) world)

  describe "pickUp" $ do
    it "doesn't change the hotbar map" $ property $
      \pickingUp ->
        \inv ->
          view hotbarMap inv `shouldBe` view hotbarMap (pickUp pickingUp inv)
    it "doesn't change the spell components if picking up use items" $ property $
      \inv ->
        \useItems ->
          view heldComponents inv `shouldBe` view heldComponents (pickUp ([], useItems) inv)
    it "doesn't change the use items if picking up spell components" $ property $
      \inv ->
        \components ->
          view heldPotions inv `shouldBe` view heldPotions (pickUp (components, []) inv)
    it "doesn't change the inventory if picking up nothing" $ property $
      \inv ->
        inv `shouldBe` pickUp ([], []) inv
    it "changes the spell components if picking up spell components" $ property $
      forAll (arbitrary `suchThat` (/= [])) $ -- Non-empty
        \components ->
          \inv ->
            view heldComponents inv `shouldNotBe` view heldComponents (pickUp (components, []) inv)
    it "changes change the use items if picking up use items" $ property $
      forAll (arbitrary `suchThat` (/= [])) $ -- Non-empty
        \useItems ->
          \inv ->
            view heldPotions inv `shouldNotBe` view heldPotions (pickUp ([], useItems) inv)
    -- Technically redundant with the last two
    it "changes the inventory if picking up something" $
      forAll (arbitrary `suchThat` (/= ([], []))) $ -- At least one Non-empty
        \pickingUp ->
          \inv ->
            inv `shouldNotBe` pickUp pickingUp inv
    it "never allows two of the same limited spell component" $ property $
      -- Since the arbitrary instance for Inventory is built on picking up stuff we just use it
      \inv ->
        [ (filter (==x) $ getLimiteds $ view heldComponents inv) | x <- getLimiteds (view heldComponents inv)] `shouldAllSatisfy` ((== 1) . length)

