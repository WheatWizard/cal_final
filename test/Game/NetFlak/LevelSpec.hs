{-# Language ScopedTypeVariables #-}
module Game.NetFlak.LevelSpec (spec) where

import Lens.Simple

import Test.Hspec
import Test.QuickCheck

import Game.NetFlak.ArbitraryInstances.Inventory
import Game.NetFlak.ArbitraryInstances.Level

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Vector

import Game.NetFlak.Level

-- A level where the player can be out of bounds
unboundedLevels :: Arbitrary a => Gen (Level a)
unboundedLevels = do
  startLevel  <- arbitrary
  newLocation <- arbitrary
  return $ set playerLocation newLocation startLevel

-- Should be a faster version of arbitrary `suchThat` inBounds lvl
arbitraryInBounds :: Level a -> Gen Vector
arbitraryInBounds lvl = do
  let
    (maxA, maxB) = view levelSize lvl
  a <- arbitrary `suchThat` (>= 0) `suchThat` (< maxA)
  b <- arbitrary `suchThat` (>= 0) `suchThat` (< maxB)
  return (a, b)
  

spec :: Spec
spec = do
  -- TODO modernize these tests
  -- TODO test inside instead
  describe "inBounds" $ do
    it "returns false for negative a" $ property $
      \ (lvl :: PuzzleLevel) ->
        \ (a, b) ->
          not $ inBounds lvl (-1 - abs a, b)
    it "returns false for negative b" $ property $
      \ (lvl :: PuzzleLevel) ->
        \ (a, b) ->
          not $ inBounds lvl (a, -1 - abs b)
    it "returns false for too large a" $ property $
      \ (lvl :: PuzzleLevel) ->
        \ (a, b) ->
          not $ inBounds lvl (abs a + fst (view levelSize lvl) + 1, b)
    it "returns false for too large b" $ property $
      \ (lvl :: PuzzleLevel) ->
        \ (a, b) ->
          not $ inBounds lvl (a, abs b + snd (view levelSize lvl) + 1)
    it "returns false for edge cases" $ property $
      \ (lvl :: PuzzleLevel) ->
        \ b ->
          not $ inBounds lvl (fst (view levelSize lvl), b)
    it "returns false for edge cases" $ property $
      \ (lvl :: PuzzleLevel) ->
        \ a ->
          not $ inBounds lvl (a, snd (view levelSize lvl))
    it "returns true for (0,0)" $ property $
      \ (lvl :: PuzzleLevel) ->
        inBounds lvl (0, 0)
    it "returns false for corner cases" $ property $
      \ (lvl :: PuzzleLevel) ->
        not $ inBounds lvl $ view levelSize lvl
    it "returns true for vectors in bounds" $ property $
      \ (lvl :: PuzzleLevel) ->
        forAll (arbitraryInBounds lvl) $
          \loc ->
            loc `shouldSatisfy` inBounds lvl
  describe "reachable" $ do
    context "when in bounds" $
      it "returns true" $ property $
        \ (lvl :: PuzzleLevel) ->
          forAll (arbitraryInBounds lvl) $
            \loc ->
              loc `shouldSatisfy` reachable lvl
  describe "putInBounds" $ do
    it "always put the player somewhere in bounds" $
      forAll unboundedLevels $
        \ (lvl :: PuzzleLevel) ->
          putInBounds lvl `shouldSatisfy` (inBounds lvl . view playerLocation)
    context "when the player was in bounds" $
      it "doesn't move the player" $ property $
        \ (lvl :: PuzzleLevel) ->
          view playerLocation lvl `shouldBe` view playerLocation (putInBounds lvl)
    it "doesn't modify things other than the player location" $
      forAll unboundedLevels $
        \ (lvl :: PuzzleLevel) ->
          lvl `shouldBe` set playerLocation (view playerLocation lvl) (putInBounds lvl)
  describe "removeFromDoor" $ do
    it "always puts the player in bounds" $
      forAll (arbitrary `suchThat` (not . null . view exitDoors)) $
        \ (lvl :: PuzzleLevel) ->
          case view exitDoors lvl of
            (door : _) ->
              let newLvl = set playerLocation (view doorPosition door) lvl in
                view playerLocation (removeFromDoor newLvl) `shouldSatisfy` inBounds newLvl
            -- Should never happen
            [] ->
              expectationFailure "This should never happen.  Something has gone horribly wrong"
