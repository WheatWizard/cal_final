module LevelSanitySpec (spec) where

import Test.Hspec

import Game.NetFlak.Files
import Game.NetFlak.Types.Level

import Data.Either

properlyFormatted :: [FilePath]
properlyFormatted =
  [ "level1.dat" -- this is the test level
  , "demo1.dat"
  , "demo2.dat"
  , "demo3.dat"
  , "demo4.dat"
  , "demo5.dat"
  , "pres1.dat"
  , "pres2.dat"
  , "pres3.dat"
  , "pres4.dat"
  , "pres5.dat"
  , "pres6.dat"
  , "pres7.dat"
  ]

improperlyFormatted :: [FilePath]
improperlyFormatted =
  [ "level2.dat"
  ]

spec :: Spec
spec = describe "Level files check" $ do
  context "Should be properly formateed" $ sequence_ (fmap buildProperFormatCheck properlyFormatted)
  context "Should be improperly formatted" $ sequence_ (fmap buildImproperFormatCheck improperlyFormatted)

buildProperFormatCheck :: FilePath -> SpecWith ()
buildProperFormatCheck file = it file $ either fst (const "") <$> safeReadLevel file `shouldReturn` ""

buildImproperFormatCheck :: FilePath -> SpecWith ()
buildImproperFormatCheck file = it file $ either (Left . fst) Right <$> safeReadLevel file `shouldReturn` Left "bad format"

safeReadLevel :: FilePath -> IO (Either (String, String) PuzzleLevel)
safeReadLevel = safeReadResource

