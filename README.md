# Netflak

Netflak is an, in development, rogue-like videogame based around the idea that magic spells can be written as computer programs.

Netflak draws inspiration from the game [Hydra-Slayer](http://www.roguetemple.com/z/hydra/) and uses the [Brain-Flak](https://esolangs.org/wiki/Main_Page) programming language for spells.

# Mechanics

Netflak's main concept is that magic is imagined as the result of a computer program being run.
In particular, each spell is a [Brain-Flak](https://esolangs.org/wiki/Main_Page) program.
We won't go into the details of Brain-Flak here but it is a very simple programming language and a great primer can be found on the esolangs page.

As the player plays through the game they collect "spell components" each a fragment of a complete program.
They can combine them together into complete programs to target enemies.
If the player casts a spell all enemies with health exactly equal to the output of the program will be killed.
The player has to try and build their spells out of their given components to match the enemy healths, but since each component takes some time to add to the spell you need to try and use your components effeciently before the enemies close in on you.

Netflak explores concepts in computability theory while providing unique puzzles.

# Compiling

Compile using

    stack build

If you wish to install the compiled binary use

    stack install

You will want to make sure the directory stack installs the binary to is on your PATH

# Running

We have two demos.

The first focuses on showing off the basic mechanic of the game.
It can be run with:

    stack exec netflak demo

in the directory of the project or, if you installed the compiled binary to your PATH,

    netflak demo

The second demo focuses on some of the more interesting mechanics available.
It can be run with the appropriate of the following:

    stack exec netflak oldDemo
    netflak oldDemo
    
We also have an endless mode which, while lacking, is kind of fun.
It can be run with the appropriate of the following:

    stack exec netflak endless
    netflak endless
    
If you are really starved for content, we have a not very fun testing level.
If you wish to try the testing scenario run the appropriate of the following:

    stack exec netflak test
    netflak test

If you or someone else made some custom levels those can be run as well.
If you want to play a custom sequence of levels, use the appropriate of the following:

    stack exec netflak <level files>
    netflak <level files>

# Controls

Hit `k` to view the controls
