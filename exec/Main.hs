import Game.NetFlak.Game

import System.Environment

main :: IO ()
main = do
  args <- getArgs
  case args of
    []          -> runFRP ["pres1.dat", "pres2.dat", "pres3.dat", "pres4.dat", "pres5.dat", "pres6.dat", "pres7.dat"]
    ["oldDemo"] -> runFRP ["demo1.dat", "demo2.dat", "demo3.dat", "demo4.dat", "demo5.dat"]
    ["demo"]    -> runFRP ["pres1.dat", "pres2.dat", "pres3.dat", "pres4.dat", "pres5.dat", "pres6.dat", "pres7.dat"]
    ["test"]    -> runFRP ["level1.dat"]
    ["render"]  -> runRenderTest
    ["endless"] -> runEndless
    ["build"]   -> runBuilder
    _           -> runFRP args

