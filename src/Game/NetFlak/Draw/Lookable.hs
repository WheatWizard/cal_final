{-# Language FlexibleInstances #-}
module Game.NetFlak.Draw.Lookable
  ( Lookable (..)
  ) where

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.SpecialArea

import Lens.Simple

class Lookable a where
  doorDescription :: Door a -> String

instance Lookable () where
  doorDescription = const "A way out of here."

instance Lookable (Maybe SpecialArea) where
  doorDescription door = case view destination door of 
    Nothing          -> "A way out of here."
    (Just Normal)    -> "A door with a small window.  You can see a ordinary room beyond."
    (Just Statue)    -> "An old gate.  You can see the statue garden beyond."
    (Just Alchemist) -> "A heavy door decorated with alchemical symbols.  It must lead to the alchemist's lab."
