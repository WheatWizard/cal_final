module Game.NetFlak.Draw.Spellbar (drawSpellbar) where

import Data.List (genericLength, groupBy, genericSplitAt, genericDrop)

import Lens.Simple

import Graphics.Vty

import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.World

import Game.NetFlak.Util
import Game.NetFlak.Window

-- Still needs to indicate what errors are offscreen
drawSpellbar :: World a -> Window
drawSpellbar world = sizedImagesWindow Center Center (pure . drawSpellBarImg . fst)
  where
    drawSpellBarImg :: Integer -> Image
    drawSpellBarImg width = maybeEllipsis <|> constructSpellImage start unclosed mismatched unopened (genericDrop start spell)
      where
        start = genericLength spell - if genericLength spell > width then width - 3 else genericLength spell
        spell = view (currentSpell . spellContents) world

        maybeEllipsis = if genericLength spell > width then string (defAttr `withForeColor` green) "..." else emptyImage

        (unclosed', mismatched', unopened') = unbalanced spell

        before :: (Num a, Eq a) => a -> a -> Bool
        before a b = b + 1 == a

        intervalToRange :: [a] -> (a, a)
        intervalToRange [] = undefined
        intervalToRange a = (head a, last a)

        unclosed   = filter ((start<=) . snd) $ intervalToRange <$> groupBy before unclosed'
        mismatched = filter ((start<=) . snd) $ intervalToRange <$> groupBy before mismatched'
        unopened   = filter ((start<=) . snd) $ intervalToRange <$> groupBy before unopened'


        constructSpellImage :: Integral a => a -> [(a, a)] -> [(a, a)] -> [(a, a)] -> String -> Image
        constructSpellImage _ [] [] [] "" = emptyImage
        constructSpellImage n unclosed mismatched unopened spell
          | (start, end) : restUnclosed <- unclosed
          , n >= start, n <= end = colorNextBlock yellow end (end - n) restUnclosed mismatched unopened
          | (start, end) : restMismatched <- mismatched
          , n >= start, n <= end = colorNextBlock red end (end - n) unclosed restMismatched unopened
          | (start, end) : restUnopened <- unopened
          , n >= start, n <= end = colorNextBlock yellow end (end - n) unclosed mismatched restUnopened
          where
            colorNextBlock :: Integral a => Color -> a -> a -> [(a, a)] -> [(a, a)] -> [(a, a)] -> Image
            colorNextBlock color end size unclosed mismatched unopened =
              let (nextChunk, rest) = genericSplitAt (size + 1) spell in
                horizJoin (string (defAttr `withForeColor` color) nextChunk) $
                  constructSpellImage (end + 1) unclosed mismatched unopened rest
        -- This last one could maybe be more efficient
        constructSpellImage n unclosed mismatched unopened (nextBrace : rest) =
            horizJoin (string (defAttr `withForeColor` green) [nextBrace]) $
              constructSpellImage (n + 1) unclosed mismatched unopened rest
    
