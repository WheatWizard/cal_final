module Game.NetFlak.Draw.Utils (
    addCharAt
  , imgFromComp
  , imgFromPotion
  , positionImage
  , positionPicture
  , extendHighlight
  ) where

import Data.Maybe (isJust)
import Data.List (genericLength)

import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.World

import Game.NetFlak.Color
import Game.NetFlak.GenericImage
import Game.NetFlak.Items
import Game.NetFlak.Util

import Lens.Simple

import Graphics.Vty

-- Takes a size, a position and an image
-- Moves the top left corner of the image to the position and then crops or pads it to the size
positionImage :: Vector -> Vector -> Image -> Image
positionImage (sizeA,sizeB) (posA,posB) = genericResize sizeA sizeB . genericTranslate posA posB

positionPicture :: Vector -> Vector -> [Image] -> [Image]
positionPicture size pos = fmap $ positionImage size pos

imgFromComp :: Integer -> World a -> SpellComponent -> Image
imgFromComp size world comp =
  case comp of
    UnlimitedSC _ ->
      let actualSize = size in
        foldl1 horizJoin [
            string (currentAttr `withForeColor` green) (trim actualSize $ itemName comp)
          , string (currentAttr `withForeColor` brightWhite) " "
          ]
    LimitedSC uses structure
      | isJust $ canUse comp world ->
        let actualSize = size in
          foldl1 horizJoin
            [ string (currentAttr `withForeColor` red) (trim actualSize $ itemName comp)
            , string (currentAttr `withForeColor` brightWhite) " "
            ]
      | otherwise ->
        let
          remuses = show $ uses - amountUsed structure (view currentSpell world)
          actualSize = size - 1 - genericLength remuses in
          foldl1 horizJoin
            [ string (currentAttr `withForeColor` blue) (trim actualSize $ itemName comp)
            , string (currentAttr `withForeColor` brightWhite) $ "x" ++ remuses
            ]

imgFromPotion :: Integer -> World a -> (Potion, Integer) -> Image
imgFromPotion size world (item, remUses) =
  horizJoin
    (string (withFontColor (itemColor item)) $ trim actualSize $ itemName item) $
      string (withForeColor currentAttr brightWhite) $ "x" ++ show remUses
  where
    actualSize = size - 1 - genericLength (show remUses)
    withFontColor = withForeColor currentAttr . getColor

extendHighlight :: Integer -> Image -> Image
extendHighlight width img =
  horizCat
    [ img
    , string currentAttr ([1 .. width - genericImageWidth img] >> " ")
    , string defAttr ""
    ]

-- Does not work for negative positions
addCharAt :: Attr -> Char -> Vector -> [Image] -> [Image]
addCharAt attr icon pos oldPic =
  case signum newPos of
    (-1, -1) ->
      charImage : uncurry genericTranslates (-newPos) oldPic
    (_, -1) ->
      genericTranslateX posA charImage : genericTranslateYs (-posB) oldPic
    (-1, _) ->
      genericTranslateY posB charImage : genericTranslateXs (-posA) oldPic
    (_, _)  ->
      uncurry genericTranslate newPos charImage : oldPic
  where
    newPos@(posA, posB) = transpose pos
    charImage = horizJoin (string attr [icon]) (string defAttr "")
