{-# OPTIONS_GHC -fno-warn-overlapping-patterns #-}

module Game.NetFlak.Draw.AddingBuildbar (drawAddingBuildbar) where

import Game.NetFlak.Types.BuildState
import Game.NetFlak.Types.Vector

import Graphics.Vty

modes :: [String]
modes = ["SpellComponent", "Potion", "Enemy"]

indexFromUI :: Adding -> Maybe Int
indexFromUI SpellComponent = Just 0
indexFromUI (Potion _)     = Just 1
indexFromUI Enemy          = Just 2
indexFromUI _              = Nothing

drawAddingBuildbar :: Maybe Adding -> Image
drawAddingBuildbar mode = foldl horizJoin emptyImage $ do
  (modeName, modeIndex) <- zip modes $ Just <$> [0..]
  let
    attr = if (indexFromUI =<< mode) == modeIndex
      then
        defAttr `withStyle` standout
      else
        defAttr
  return $ horizJoin (string attr modeName) (string defAttr " ")
  
