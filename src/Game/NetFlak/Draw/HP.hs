module Game.NetFlak.Draw.HP where

import Game.NetFlak.Window

import Game.NetFlak.Types.Player
import Game.NetFlak.Types.World

import Graphics.Vty

import Lens.Simple

drawHP :: World a -> Window
drawHP world = imagesWindow Min Min
  [ vertCat [
      string defAttr ""
    , string hpAttr $ show hpLeft ++ "/" ++ show maxHP
    , string defAttr ""
    ]
  ]
  where
    hpLeft = max 0 $ maxHP - view (player . damageTaken) world
    maxHP  = view (player . maxHealth) world
    hpAttr = defAttr `withForeColor`
      case view (player . damageTaken) world of
        dmg
          | dmg == 0                 -> green
          | dmg <= maxHP `div` 4     -> brightGreen
          | dmg <= maxHP `div` 2     -> brightYellow
          | dmg <= 3 * maxHP `div` 4 -> yellow
          | dmg <  maxHP             -> brightRed
          | otherwise                -> red
