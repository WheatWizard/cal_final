module Game.NetFlak.Draw.CompInventory (compInventoryWindow) where

import Control.Monad.State.Lazy (get)

import Data.Map (Map, toList)

import Graphics.Vty

import Game.NetFlak.Types.World
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.Inventory

import Game.NetFlak.Window
import Game.NetFlak.WindowMonad

import Game.NetFlak.Draw.Utils

import Lens.Simple

compInventoryWindow :: Bool -> Integer -> World a -> WindowMonad [Image]
compInventoryWindow selected cursorIx world = do
  (_, (width, _)) <- get
  setPicture $ drawCompInventory width selected cursorIx world

getHotkey :: Map Char Integer -> Integer -> Char
getHotkey map index = case filter ((== index) . snd) (toList map) of
  ((key, _) : _) -> key
  []             -> ' '

drawCompInventory :: Integer -> Bool -> Integer -> World a -> Window
drawCompInventory width selected cursorIx world = imagesWindow Min Min $
  pure $ vertCat $                                                      -- Combine vertically
    zipWith drawCursor [0..] $                                          -- handle the cursor
      (extendHighlight (width - 2) . imgFromComp (width - 2) world) <$> -- Create images
        view (player . playerInv . heldComponents) world                -- Get the comps
  where
    drawCursor ix
      | ix == cursorIx, selected = (string (defAttr `withStyle` standout) buffer <|>)
      | otherwise                = (string defAttr buffer <|>)
      where
        hotkey = getHotkey (view (player . playerInv . hotbarMap) world) ix
        buffer = hotkey : " "

