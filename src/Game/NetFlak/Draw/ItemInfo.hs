module Game.NetFlak.Draw.ItemInfo (drawItemInfo) where

import Graphics.Vty

import Game.NetFlak.Color
import Game.NetFlak.Items
import Game.NetFlak.Window

drawItemInfo :: (Item a) => Maybe a -> Window
drawItemInfo Nothing  = imagesWindow Center Center [ emptyImage ]
drawItemInfo (Just i) = imagesWindow Center Center [ horizJoin (string (defAttr `withForeColor` (getColor $ itemColor i)) (itemName i)) (string defAttr (": " ++ itemDesc i)) ]
