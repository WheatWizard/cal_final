module Game.NetFlak.Draw.Box where

import Game.NetFlak.Types.Vector

import Graphics.Vty

drawBox :: Vector -> Image
drawBox (width,height) =
  charDef '┏' <|> charFillDef '━' (w-2) 1 <|> charDef '┓'
  <->
  charFillDef '┃' 1 (h-2) <|> charFillDef ' ' (w-2) (h-2) <|> charFillDef '┃' 1 (h-2)
  <->
  charDef '┗' <|> charFillDef '━' (w-2) 1 <|> charDef '┛'
  where
    w = fromInteger width
    h = fromInteger height
    charDef = char defAttr
    charFillDef = charFill defAttr
