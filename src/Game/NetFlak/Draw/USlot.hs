module Game.NetFlak.Draw.USlot (uSlotWindow) where

import Prelude hiding (lookup)

import Control.Monad.State.Lazy (get)

import Data.Map.Strict (lookup)

import Graphics.Vty

import Game.NetFlak.Types.World
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.Inventory
import Game.NetFlak.Types.Potion

import Game.NetFlak.Draw.Utils

import Game.NetFlak.Window
import Game.NetFlak.WindowMonad

import Lens.Simple

uSlotWindow :: World a -> WindowMonad [Image]
uSlotWindow world = do
  (_, (width, _)) <- get
  setPicture $ imagesWindow Min Min $ drawUSlot width world

drawUSlot :: Integer -> World a -> [Image]
drawUSlot width world =
  case view (player . playerInv . quickPotion) world of
    Nothing -> []
    Just uItem ->
      case lookup uItem $ view (player . playerInv . heldPotions) world of
        Nothing   -> []
        Just uses -> [ horizJoin (string defAttr "U ") (imgFromPotion (width - 2) world (uItem, uses)) ]

