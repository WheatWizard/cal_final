module Game.NetFlak.Draw.Controls (drawControls) where

import Graphics.Vty

import Game.NetFlak.Types.World
import Game.NetFlak.Types.Settings
import Game.NetFlak.Types.Control

import Game.NetFlak.Window

import Lens.Simple

showControl :: Control -> String
showControl (Move (-1, 0)) = "Move up"
showControl (Move ( 1, 0)) = "Move down"
showControl (Move (0, -1)) = "Move right"
showControl (Move (0,  1)) = "Move left"
showControl (Move (0,  0)) = "Take a turn"
showControl (Menu 1)       = "Menu down"
showControl (Menu (-1))    = "Menu up"
showControl (Crement (-1)) = "Decrement"
showControl (Crement 1)    = "Increment"
showControl x = show x

showEvent :: (Key, [Modifier]) -> String
showEvent (KChar '\t', []) = "Tab"
showEvent (KChar x, [])    = x : ""
showEvent (KEnter, [])     = "Enter"
showEvent (KEsc, [])       = "Escape"
showEvent x = show x

drawControls :: World a -> Window
drawControls world = imagesWindow Min Min $ (:[]) $ vertCat $ do
  (event, control) <- view (userSettings . userControls) world
  return $ string defAttr $ showEvent event ++ ": " ++ showControl control
  
  
