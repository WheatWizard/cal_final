module Game.NetFlak.Draw.Hotbar (hotbarWindow) where

import Control.Monad.State.Lazy (get)

import Data.List (genericIndex, genericLength)
import Data.Map (toList)

import Graphics.Vty

import Lens.Simple

import Game.NetFlak.Window
import Game.NetFlak.WindowMonad

import Game.NetFlak.Types.Inventory
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.Settings
import Game.NetFlak.Types.World

import Game.NetFlak.Draw.Utils

hotbarWindow :: World a -> WindowMonad [Image]
hotbarWindow world = do
  (_, (width, _)) <- get
  setPicture $ imagesWindow Min Min $ drawHotbar width world
  
drawHotbar :: Integer -> World a -> [Image]
drawHotbar width world = do
  (lineNum, (key, index)) <- zip [0..] $ toList $ view (player . playerInv . hotbarMap) world
  let
    textPosition = (0, lineNum)
    boxSize      = (view (userSettings . hotbarWidth) world, lineNum + 1)
  fmap (positionImage boxSize textPosition) $
    case view (player . playerInv . heldComponents) world of
      heldComponents
        | index < genericLength heldComponents ->
          return $ horizCat
            [ string defAttr $ key : " "
            ,  imgFromComp (width - 2) world $ genericIndex heldComponents index
            ]
        | otherwise -> []
