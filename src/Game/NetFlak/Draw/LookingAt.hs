module Game.NetFlak.Draw.LookingAt (lookingAtWindow) where

import Control.Monad (guard)
import Control.Monad.State.Lazy (get)

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.RichString
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.World

import Game.NetFlak.Enemy
import Game.NetFlak.Items
import Game.NetFlak.Level
import Game.NetFlak.RichString
import Game.NetFlak.Util
import Game.NetFlak.Window
import Game.NetFlak.WindowMonad

import Game.NetFlak.Draw.Lookable
import Game.NetFlak.Draw.Utils

import Lens.Simple

import Graphics.Vty

-- No wrapping yet
lookingAtWindow ::
  Lookable a
   => Vector -> World a -> WindowMonad [Image]
lookingAtWindow center world = do
  (_, (width, _)) <- get
  let 
    -- An image describing the components at the pointer
    compImgs = do
      (location, item) <- view (currentLevel . floorComponents) world
      guard $ location == center
      guard $ inBounds (view currentLevel world) location
      return $ horizJoin (imgFromComp width world item) $ string defAttr $ ": " ++ itemDesc item
    -- An image describing the use items at the pointer
    useImgs = do
      (location, item) <- view (currentLevel . floorPotions) world
      guard $ location == center
      guard $ inBounds (view currentLevel world) location
      return $ horizJoin (imgFromPotion width world (item, 1)) $ string defAttr $ ": " ++ itemDesc item
    -- An image describing the enemies at the pointer
    enemyImgs = do
      (location, enemy) <- view (currentLevel . enemies) world
      guard $ location == center
      guard $ inBounds (view currentLevel world) location
      return $ richString $ RichString $
        case view statueHealth enemy of
          Nothing ->
            [ (iconColorPID enemy, sanitize $ view name enemy)
            , ( DefaultPID
              , concat
                [ ": Has "
                , show $ view enemyHealth enemy
                , " health and deals "
                , show $ view enemyDamage enemy
                , " damage."
                ]
              )
            ]
          Just s ->
            [ (iconColorPID enemy, "Statue of a " ++ sanitize (view name enemy))
            , ( DefaultPID
              , concat 
                [ ": A statue, it will be awoken by "
                , show s
                , "."
                ]
              )
            ]
    -- An image describing all enemy remains at the pointer
    deadImgs = do
      (location, enemy) <- view deadEnemies world
      guard $ location == center
      guard $ inBounds (view currentLevel world) location
      return $ richString $ RichString
        [ (DefaultPID, "A pile of ")
        , (view iconPColor enemy, view name enemy)
        , (DefaultPID, " remains.")
        ]
    -- An image if the player is at the current location
    playerImgs = do
      guard $ view (currentLevel . playerLocation) world == center
      return $ string (defAttr `withForeColor` cyan) "You"
    -- A description of the door or lack thereof
    doorImgs = do
      door <- view (currentLevel . exitDoors) world
      -- Must be looking at the door
      guard $ view doorPosition door == center
      -- Do not describe doors that are not reachable
      guard $ reachable (view currentLevel world) $ view doorPosition door
      return $ string defAttr $ if view isOpen door
        then doorDescription door
        else if inBounds (view currentLevel world) center
          then "There is a crack in the floor."
          else "There is a crack in the wall."
  setPicture $ imagesWindow Min Min $ pure $
    vertCat $ -- Join all the different descriptions into a picture
      compImgs ++ useImgs ++ enemyImgs ++ deadImgs ++ playerImgs ++ doorImgs
