module Game.NetFlak.Draw.Messages where

import Data.List (genericDrop)

import Game.NetFlak.RichString
import Game.NetFlak.Window

import Game.NetFlak.Types.RichString
import Game.NetFlak.Types.World

import Graphics.Vty

import Lens.Simple

drawMessages :: Integer -> World a -> Window
drawMessages n world = imagesWindow Min Max $ pure $
  vertCat $ reverse $ genericDrop n $ richString <$> view richLog world
