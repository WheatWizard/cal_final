module Game.NetFlak.Draw.Settings (drawSettings) where

import Graphics.Vty

import Game.NetFlak.Types.World
import Game.NetFlak.Types.Settings

import Game.NetFlak.Window

import Lens.Simple

settingsList = [
    ("Max spell cycles", maxSpellCycles)
  , ("Hotbar width", hotbarWidth)
  , ("Spellbar width", spellbarWidth)
  , ("Message window width", messageWidth)
  , ("Enemybar width", enemybarWidth)
  , ("Infobar width", infobarWidth)
  ]

drawSettings :: Integral a => a -> World b -> Window
drawSettings index world = imagesWindow Min Min $ (:[]) $ vertCat $ do
  (lineNum, (name, setting)) <- zip [0..] $ settingsList
  let
    lineAttr = if lineNum == index
      then
        defAttr `withStyle` standout
      else
        defAttr
  return $ string lineAttr $ name ++ ": " ++ show (view (userSettings . setting) world)
  
  
