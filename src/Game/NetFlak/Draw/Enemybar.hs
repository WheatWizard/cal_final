module Game.NetFlak.Draw.Enemybar (drawEnemybar) where

import Data.List (intercalate)

import Graphics.Vty

import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.World

import Game.NetFlak.Color
import Game.NetFlak.Enemy
import Game.NetFlak.Items
import Game.NetFlak.Util
import Game.NetFlak.Level (enemiesInBounds)

import Lens.Simple

drawEnemybar :: Maybe Integer -> World a -> Image
drawEnemybar highlight world = foldl vertJoin emptyImage $ do
  (index, (_, enemy)) <- zip [0..] $ enemiesInBounds $ view currentLevel world
  let
    startAttr = if Just index == highlight
      then
        defAttr `withStyle` standout
      else
        defAttr
    icon   = string (startAttr `withForeColor` (getColor $ iconColorPID enemy)) [ sanitizeChar $ view iconChar enemy ]
    space  = string (startAttr `withForeColor` red) " "
    statHealth = case view statueHealth enemy of
      Nothing -> emptyImage
      Just h  -> string (startAttr `withForeColor` brightBlack) $ '%' : show h
    stack  = case view enemyStack enemy of
      []  -> emptyImage
      eS  -> string (startAttr `withForeColor` green)  $ ':' : intercalate "," (map show eS)
    growth = case view enemyGrowth enemy of
      NoChange    -> emptyImage
      Geometric n -> string (startAttr `withForeColor` magenta) $ '*' : show n
      Linear    n -> string (startAttr `withForeColor` magenta) $ '+' : show n
    health = string (startAttr `withForeColor` yellow) $ 'v' : show (view enemyHealth enemy)
    damage = case view enemyDamage enemy of
      0   -> emptyImage
      eD  -> string (startAttr `withForeColor` red)    $ '#' : show eD
    effects = case view attackEffects enemy of
      [] -> emptyImage
      aE -> horizCat 
        [ string (startAttr `withForeColor` (getColor $ itemColor effect)) $
          '&' : case effect of
            HealPot     -> "H"
            HPUp        -> "h"
            Noitop      -> "q"
            Mirror      -> "M"
            GrowthPot   -> "G"
            WitherPot   -> "W"
            CollatzPot  -> "C"
            StrengthPot -> "c"
            WeakPot     -> "F"
            _           -> "?"
        | effect <- aE
        ]
  return $ horizCat 
    [ icon
    , space
    , statHealth
    , stack
    , growth
    , health
    , damage 
    , effects
    ]
