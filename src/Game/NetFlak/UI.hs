module Game.NetFlak.UI 
  ( runDisplay
  ) where

import Data.Function (on)

import Control.Concurrent
import Control.Concurrent.STM

import Control.Monad (join, unless)

import Graphics.Vty hiding (Event)

import Reactive.Banana
import Reactive.Banana.Frameworks

import Game.NetFlak.Types.CurseEvent

runDisplay :: Vty -> (Event CurseEvent -> MomentIO (Behavior Picture, Event (Vty -> IO ()))) -> IO ()
runDisplay vty graph = do
  (curseEventH, curseEventFire) <- newAddHandler
  let
    networkDescription :: MomentIO ()
    networkDescription = do
      curseEventE <- fromAddHandler curseEventH
      (screen,actions) <- graph curseEventE
      let shutdownCheck = (isShutdown vty >>=) . flip unless
      displayUpdates <- changes $ shutdownCheck . update vty <$> screen
      reactimate $ shutdownCheck . ($vty) <$> actions
      reactimate' displayUpdates
  network <- compile networkDescription
  actuate network
  (screenA,screenB) <- displayBounds $ outputIface vty
  curseEventFire $ EvResize screenA screenB
  let
    loop :: IO ()
    loop = do
      stop <- isShutdown vty
      unless stop $ nextEvent vty >>= curseEventFire >> loop
  loop
