module Game.NetFlak.RichString where

import Graphics.Vty

import Game.NetFlak.Color

import Game.NetFlak.Types.RichString

rawContent :: RichString -> String
rawContent (RichString s) = concatMap snd s

richString :: RichString -> Image
richString (RichString chunks) = horizCat $
  uncurry (string . (currentAttr `withForeColor`) . getColor) <$> chunks
