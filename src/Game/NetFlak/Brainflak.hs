module Game.NetFlak.Brainflak (brainflakValue, brainflakValueFrom) where

import Data.List (genericLength,genericTake)

data Token a
  = Start
  | End
  | One
  | StackHeight
  | Pop
  | StackSwap
  | OpenPush
  | ClosePush
  | OpenNegate
  | CloseNegate
  | OpenLoop a
  | CloseLoop a
  | OpenZero
  | CloseZero
  deriving (Eq,Show)

data Brainflak a b = Brainflak
  { pastProgram   :: [Token a]
  , futureProgram :: [Token a]
  , onStack       :: [b]
  , offStack      :: [b]
  , thirdStack    :: [b]
  } deriving Show 

sanitize :: String -> String
sanitize = filter (flip elem "()[]<>{}")

newBrainflakFromStack :: (Integral a, Integral b) => [b] -> String -> Brainflak a b
newBrainflakFromStack startStack prog = Brainflak
  { pastProgram   = []
  , futureProgram = parse $ sanitize prog
  , onStack       = startStack
  , offStack      = []
  , thirdStack    = []
  }

newBrainflak :: (Integral a, Integral b) => String -> Brainflak a b
newBrainflak = newBrainflakFromStack []

-- TODO make this robust to parse errors
nextToken :: Integral a => String -> (String, Token a)
nextToken ('(':')':str) = (str, One)
nextToken ('[':']':str) = (str, StackHeight)
nextToken ('<':'>':str) = (str, StackSwap)
nextToken ('(':str)     = (str, OpenPush)
nextToken (')':str)     = (str, ClosePush)
nextToken ('[':str)     = (str, OpenNegate)
nextToken (']':str)     = (str, CloseNegate)
nextToken ('<':str)     = (str, OpenZero)
nextToken ('>':str)     = (str, CloseZero)

parse :: Integral a => String -> [Token a]
parse str = Start : parse' 1 str [End]
  where
    parse' :: Integral a => a -> String -> [Token a] -> [Token a]
    parse' _ "" = id
    parse' n ('{':'}':str) = (Pop:) . parse' (n+1) str
    parse' n ('}':str) = (CloseLoop (negate n):) . parse' (n+1) str
    parse' n ('{':str) =
      let (len, remStr, tokens) = parseLoop 1 str in
        (OpenLoop len:) . tokens . parse' (n+len+1) remStr
    parse' n str =
      let (remStr, token) = nextToken str in
        (token :) . parse' (n+1) remStr

    parseLoop :: Integral a => a -> String -> (a, String, [Token a] -> [Token a])
    parseLoop n "" = (n, "", id)
    parseLoop n ('{':'}':str) =
      let (len, restStr, tokens) = parseLoop (n+1) str in
        (len, restStr, (Pop:) . tokens)
    parseLoop n ('}':str) = (n, str, (CloseLoop (negate n):))
    parseLoop n ('{':str) =
      let (loopLen, remStr, tokens) = parseLoop 1 str in
        let (totalLen, restRemStr, restTokens) = parseLoop (n+loopLen+1) remStr in
          (totalLen, restRemStr, (OpenLoop loopLen:) . tokens . restTokens)
    parseLoop n str =
      let (remStr, token) = nextToken str in
        let (len, restStr, tokens) = parseLoop (n+1) remStr in
          (len, restStr, (token:) . tokens)

brainflakValue :: (Integral a, Integral b) => String -> a -> Maybe b
brainflakValue = brainflakValueFrom []

brainflakValueFrom :: (Integral a, Integral b) => [b] ->  String -> a -> Maybe b
brainflakValueFrom startStack prog n = case ranProg of
  []       -> Nothing
  (prog:_) -> Just $ readStackTop $ thirdStack prog
  where
    ranProg = dropWhile ((/=End) . head . futureProgram) $ genericTake n $ iterate step $ newBrainflakFromStack startStack prog

mapStackTop :: Num a => (a -> a) -> [a] -> [a]
mapStackTop f []     = [f 0]
mapStackTop f (x:xs) = f x : xs

readStackTop :: Num a => [a] -> a
readStackTop []    = 0
readStackTop (x:_) = x

rollProgram :: Integral a => a -> Brainflak b c -> Brainflak b c
rollProgram n bf
  | n == 0 = bf
  | n >  0 = rollProgram (n-1) bf { pastProgram = head (futureProgram bf) : pastProgram bf, futureProgram = tail $ futureProgram bf }
  | n <  0 = rollProgram (n+1) bf { pastProgram = tail $ pastProgram bf, futureProgram = head (pastProgram bf) : futureProgram bf }

step :: (Integral a, Num b, Eq b) => Brainflak a b -> Brainflak a b
step bf =
  case head $ futureProgram bf of
    Start -> advancedBf
    End -> bf
    One -> advancedBf { thirdStack = mapStackTop (+1) $ thirdStack advancedBf }
    StackHeight -> advancedBf { thirdStack = mapStackTop (+genericLength (onStack advancedBf)) $ thirdStack advancedBf }
    Pop -> advancedBf { onStack = drop 1 $ onStack advancedBf, thirdStack = mapStackTop (+readStackTop (onStack advancedBf)) $ thirdStack advancedBf }
    StackSwap -> advancedBf { onStack = offStack advancedBf, offStack = onStack advancedBf }
    OpenPush -> advancedBf { thirdStack = 0 : thirdStack advancedBf }
    ClosePush -> advancedBf { onStack = readStackTop (thirdStack advancedBf) : onStack advancedBf, thirdStack = mapStackTop (+ readStackTop (thirdStack advancedBf)) $ drop 1 $ thirdStack advancedBf }
    OpenNegate -> advancedBf { thirdStack = 0 : thirdStack advancedBf }
    CloseNegate -> advancedBf { thirdStack = mapStackTop (subtract $ readStackTop (thirdStack advancedBf)) $ drop 1 $ thirdStack advancedBf }
    OpenLoop n
      | readStackTop (onStack advancedBf) == 0 -> rollProgram n advancedBf
      | readStackTop (onStack advancedBf) /= 0 -> advancedBf
    CloseLoop n
      | readStackTop (onStack advancedBf) == 0 -> advancedBf
      | readStackTop (onStack advancedBf) /= 0 -> rollProgram n advancedBf
    OpenZero -> advancedBf { thirdStack = 0 : thirdStack advancedBf }
    CloseZero -> advancedBf { thirdStack = drop 1 $ thirdStack advancedBf }
  where advancedBf = rollProgram 1 bf
