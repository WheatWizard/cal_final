module Game.NetFlak.Window 
  ( Alignment (Min,Center,RelativeCenter,Max)
  , Side (..)
  , Window ()
  , imagesWindow
  , sizedImagesWindow
  , constructWindow
  , glueLeft, glueRight, glueBottom, glueTop
  , glueHoriz, glueVert
  ) where

import Data.Maybe (fromMaybe)

import Game.NetFlak.GenericImage
import Game.NetFlak.Types.Vector

import Graphics.Vty

data Alignment
  = Min
  | Center
  | RelativeCenter Integer
  | Max
  deriving Show
data Side
  = North
  | South
  | East
  | West

data Window
  = PictureW { pic :: Vector -> [Image], horizAlign :: Alignment, vertAlign :: Alignment }
  | Glue { gluer :: Window, gluee :: Window, gluerWidth :: Maybe Integer, side :: Side}

imagesWindow :: Alignment -> Alignment -> [Image] -> Window
imagesWindow horizAlign vertAlign images = PictureW (const images) horizAlign vertAlign

-- Build a Window based on an image whose content is dependent on the Window size
sizedImagesWindow :: Alignment -> Alignment -> (Vector -> [Image]) -> Window
sizedImagesWindow horizAlign vertAlign sizedImages = PictureW sizedImages horizAlign vertAlign

-- Glues the first window to the left of the second and fixes the first windows width
glueLeft :: Window -> Integer -> Window -> Window
glueLeft gluer gluerWidth gluee = Glue gluer gluee (Just gluerWidth) West

-- Glues the first window to the right of the second and fixes the first windows width
glueRight :: Window -> Integer -> Window -> Window
glueRight gluer gluerWidth gluee = Glue gluer gluee (Just gluerWidth) East

-- Glues the first window to the bottom of the second and fixes the first windows height
glueBottom :: Window -> Integer -> Window -> Window
glueBottom gluer gluerHeight gluee = Glue gluer gluee (Just gluerHeight) South

-- Glues the first window to the top of the second and fixes the first windows height
glueTop :: Window -> Integer -> Window -> Window
glueTop gluer gluerHeight gluee = Glue gluer gluee (Just gluerHeight) North

-- Glues the two windows horizontally, with the first window on the left
glueHoriz :: Window -> Window -> Window
glueHoriz left right = Glue left right Nothing West

-- Glues the two windows vertically with the first window on top
glueVert :: Window -> Window -> Window
glueVert top bottom = Glue top bottom Nothing North

{-
Properties of windows:
* Can be a plain wrapper around [Image]
  * Has an internal alignment both horizontal and vertical
* Otherwise can be the result of glueing together other windows
  * Can be glued together evenly or one can have a fixed width/height (depending on the direction of the glue)
  * The glue line will always pass through the whole window that that is fine
-}

constructWindow :: Vector -> Window -> [Image]
constructWindow (width,height) (PictureW picture hAlign vAlign) = genericCrops width height $
  case hAlign of
    Min                -> vAligned
    Center             -> genericTranslateXs (width `div` 2 - pictureWidth `div` 2) vAligned
    RelativeCenter cen -> genericTranslateXs (width `div` 2 - cen) vAligned
    Max                -> genericTranslateXs (width - pictureWidth) vAligned
  where
    vAligned = case vAlign of
      Min                -> sizedPic
      Center             -> genericTranslateYs (height `div` 2 - pictureHeight `div` 2) sizedPic
      RelativeCenter cen -> genericTranslateYs (height `div` 2 - cen) sizedPic
      Max                -> genericTranslateYs (height - pictureHeight) sizedPic
    pictureWidth  = genericImageWidths  sizedPic
    pictureHeight = genericImageHeights sizedPic
    sizedPic = picture (width,height)
constructWindow (width,height) (Glue gluer gluee gluerWidth side) =
  case side of
    North  -> horizontalGluer ++ genericTranslateYs exGluerHeight horizontalGluee
    South  -> horizontalGluee ++ genericTranslateYs (height - exGluerHeight) horizontalGluer
    West   -> verticalGluer   ++ genericTranslateXs exGluerWidth verticalGluee
    East   -> verticalGluee   ++ genericTranslateXs (width - exGluerWidth) verticalGluer
  where
    exGluerWidth = fromMaybe (width `div` 2) gluerWidth
    exGluerHeight = fromMaybe (height `div` 2) gluerWidth
    horizontalGluer = constructWindow (width, exGluerHeight) gluer
    horizontalGluee = constructWindow (width, height - exGluerHeight) gluee
    verticalGluer = constructWindow (exGluerWidth, height) gluer
    verticalGluee = constructWindow (width - exGluerWidth, height) gluee
