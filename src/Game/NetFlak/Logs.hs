module Game.NetFlak.Logs
  ( fillLog
  , logAction
  , attemptSpell
  , getDoorMessages
  ) where 

import Control.Monad.Random

import Lens.Simple

import Game.NetFlak.Brainflak
import Game.NetFlak.Color
import Game.NetFlak.Enemy
import Game.NetFlak.Items
import Game.NetFlak.Level
import Game.NetFlak.RichString
import Game.NetFlak.Util

import Game.NetFlak.Generator.Util

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyAction
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.LogItem
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.RichString
import Game.NetFlak.Types.SpecialArea
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.Potion

attemptSpell :: Spell -> [Integer] -> Integer -> Either LogItem Integer
attemptSpell spell startStack timeout
  | not $ isBalanced $ view spellContents spell = Left UnbalancedBraces
  | Nothing  <- result = Left TimeOut
  | Just val <- result = Right val
  where
    result = brainflakValueFrom startStack (view spellContents spell) timeout

getDoorMessage :: Level a -> Door a -> LogItem
getDoorMessage lvl door
  | reachable lvl $ view doorPosition door = DoorOpens
  | otherwise = DistantDoorOpens

getDoorMessages :: Level a -> [LogItem]
getDoorMessages lvl = do
  door <- view exitDoors lvl
  -- Door is not already open
  guard $ not $ view isOpen door
  -- The door has been triggered
  guard $ case view openCondition door of
    Always     -> True
    LevelClear -> null $ view enemies lvl
    Never      -> False
  return $ getDoorMessage lvl door

logAction :: EnemyAction -> Enemy -> [ LogItem ]
logAction (AttackAction dmg potions) enemy = [ EnemyAttack enemy dmg potions ]
logAction (Death cOD) enemy = [ EnemyDeath cOD enemy ]
logAction _ _ = []

fillLog ::
  MonadRandom m
    => LogItem -> m [ RichString ]
fillLog PlayerDies = pure <$> choose
  [ RichString
    [ (CyanPID,    "Your")
    , (DefaultPID, " vision fades to black.")
    ]
  , RichString
    [ (CyanPID,    "You")
    , (DefaultPID, " give into the pain.")
    ]
  , RichString
    [ (CyanPID,    "You")
    , (DefaultPID, " are already dead.")
    ]
  , RichString
    [ (CyanPID,    "You")
    , (DefaultPID, " frantically scribble a last will with ")
    , (CyanPID,    "your")
    , (DefaultPID, " dying breath.")
    ]
  , RichString
    [ (DefaultPID, "It is over.")
    ]
  ]
fillLog (EnemyAttack enemy 0 []) = do
  action <- choose [
      "swung at you but missed"
    , "stared menacingly at you"
    , "growled at you"
    ]
  return $ pure $ RichString [
      (DefaultPID, "The ")
    , (iconColorPID enemy, sanitize $ view name enemy)
    , (DefaultPID, " " ++ action ++ ".")
    ]
fillLog (EnemyAttack enemy dmg potions) = do
  hitType <- choose ["struck", "hit", "bit", "punched"]
  return $
    [ RichString $
      [ (DefaultPID, "The ")
      , (iconColorPID enemy, sanitize $ view name enemy)
      , (DefaultPID, " " ++ hitType)
      , (CyanPID, " you")
      , (DefaultPID, ", dealing " ++ show dmg ++ " damage.")
      ]
    , RichString $
      [ (DefaultPID, " ")
      , (CyanPID,    "You")
      , (DefaultPID, "'ve been poisoned!")
      ]
    ] ++ fmap RichString
      [
        [ (CyanPID,    "You")
        , (DefaultPID, " feel the effects of ")
        , (itemColor potion, itemName potion)
        , (DefaultPID, ".")
        ]
      | potion <- potions
      ]
fillLog (EnemyDeath PlayerKill enemy) = do
  explosionResult <- choose
    [ "a pile of gore"
    , "chunks"
    ]
  deathType <- choose
    [ "exploded into " ++ explosionResult
    , "disappeared in a puff of smoke"
    , "collapsed on the ground"
    , "melted into a puddle of goo"
    , "died peacefully in its sleep"
    ]
  deathAction <- chooseM
    [ return $ RichString
      [ (CyanPID,    "You")
      , (DefaultPID, " struck the ")
      , (iconColorPID enemy, sanitize $ view name enemy)
      , (DefaultPID, " down with a bolt of lightning.")
      ]
    , do
        attack <- choose
          [ "a fireball"
          , "a bolt of lightning"
          ]
        return $ RichString
          [ (CyanPID,    "You")
          , (DefaultPID, " hurled " ++ attack ++ " at the ")
          , (iconColorPID enemy, sanitize $ view name enemy)
          , (DefaultPID, " exploding it into " ++ explosionResult ++ ".")
          ]
    , return $ RichString
      [ (DefaultPID, "As ")
      , (CyanPID,    "you")
      , (DefaultPID, " were muttering incantations, the ")
      , (iconColorPID enemy, sanitize $ view name enemy)
      , (DefaultPID, " had an unrelated heart attack.")
      ]
    , return $ RichString
      [ (DefaultPID, "With a single wave of ")
      , (CyanPID, "your hand")
      , (DefaultPID, " all the blood in the ")
      , (iconColorPID enemy, sanitize $ view name enemy)
      , (DefaultPID, " evaporated.")
      ]
    ]
  pure <$> choose
    [ RichString
      [ (DefaultPID, "The ")
      , (iconColorPID enemy, sanitize $ view name enemy)
      , (DefaultPID, " " ++ deathType ++ ".")
      ]
    , deathAction
    ]
fillLog (EnemyDeath Trampled enemy) = return $ pure $ RichString
  [ (DefaultPID, "The ")
  , (iconColorPID enemy, sanitize $ view name enemy)
  , (DefaultPID, " was trampled to death.")
  ]
fillLog (EnemyDeath InWall enemy) = return $ pure $ RichString
  [ (DefaultPID, "Faint Sceams can be heard from beyond the walls.")
  ]
fillLog (StatueAwakens enemy) = return $ pure $ RichString
  [ (CyanPID,    "Your")
  , (DefaultPID, " words awaken the ")
  , (iconColorPID enemy, view name enemy)
  , (DefaultPID, " from their slumber.")
  ]
fillLog (SpellCast spell)
  | view spellContents spell == "" =
    return $ pure $ RichString
      [ (CyanPID, "You")
      , (DefaultPID, " cast an empty spell.")
      ]
  | otherwise = 
    return $ pure $ RichString
      [ (CyanPID, "You")
      , (DefaultPID, " cast ")
      , (GreenPID, view spellContents spell)
      , (DefaultPID, ".")
      ]
fillLog TimeOut = return $ pure $ RichString [(DefaultPID, "Your spell timed out before it completed!")]
fillLog UnbalancedBraces = return $ pure $ RichString [(DefaultPID, "Your spell has unbalanced braces!")]
fillLog (Pickup colorID name) = return $ pure $ RichString
  [ (CyanPID, "You")
  , (DefaultPID, " picked up ")
  , (colorID, name)
  , (DefaultPID, ".")
  ]
fillLog (UsePotion potion) = do
  drinkAct <- choose
    [ "take a swig of"
    , "take a gulp of"
    , "down"
    , "imbibe"
    , "chug"
    , "drink"
    , "knock back"
    ]
  fruit  <- choose
    [ "grape"
    , "watermelon"
    , "sour apple"
    , "strawberry"
    , "cloudberry"
    , "cherry"
    , "banana kiwi"
    , "blueberry"
    , "pineapple"
    ]
  flavor <- choose
    [ "strangely sweet"
    , "quite bitter"
    , "quite bland"
    , "awful"
    , "like cinnamon"
    , "like " ++ fruit
    , "tangy"
    , "disgustingly sweet"
    , "absolutely vile"
    , "tannic"
    , "fruity"
    , "metallic"
    ]
  aftertaste <- choose
    [ "a bitter"
    , "a strong"
    , "an astringent"
    , "a metallic"
    , "a bit of an"
    ]
  oneOff <- choose
    [ "It's carbonated!"
    , "Bottoms up!"
    , "It burns your throat a little on the way down."
    , "You throw up in your mouth a bit."
    , "It feels light and airy in your mouth."
    , "A few drops spill on your clothes."
    , "It's " ++ fruit ++ " flavored!"
    ]
  tasteString <- choose
    [ "It tastes " ++ flavor ++ "."
    , "It has " ++ aftertaste ++ " aftertaste."
    , oneOff
    ]
  return $ pure $ RichString
    [ (CyanPID, "You ")
    , (DefaultPID, drinkAct ++ " the ")
    , (itemColor potion, itemName potion)
    , (DefaultPID, ". " ++ tasteString)
    ]
fillLog (BadPotion potion) = do
  drinkMessage <- fillLog (UsePotion potion)
  return $ drinkMessage ++
    [ RichString
      [ (DefaultPID, "However nothing happens. Perhaps the wizard who designed this potion forgot to implement its functionality.")
      ]
    ]
fillLog (CannotUse (RichString itemName) (RichString reason)) = return $ pure $ RichString $
  itemName ++ [(DefaultPID, " can't be used because ")] ++ reason ++ [(DefaultPID, ".")]
fillLog DoorOpens = return $ pure $ RichString [(DefaultPID, "Clunk! A door opens.")]
fillLog DistantDoorOpens = return $ RichString <$>
  [
    [ (CyanPID,    "You")
    , (DefaultPID, " hear a distant mechanical sound from beyond the walls. ")
    ]
  , [ (CyanPID,    "You")
    , (DefaultPID, " have a sinking feeling ")
    , (CyanPID,    "you")
    , (DefaultPID, " are trapped.")
    ]
  ]
fillLog (AreaBanner Normal)    = return $ pure $ RichString [(DefaultPID, "Room")]
fillLog (AreaBanner Alchemist) = return $ pure $ RichString [(RedPID, "Alchemist's Lab")]
fillLog (AreaBanner Statue)    = return $ pure $ RichString [(GreyPID, "Statue Garden")]
