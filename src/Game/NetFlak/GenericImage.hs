module Game.NetFlak.GenericImage where

import Data.Function (on)

import Graphics.Vty

genericCrop :: (Integral a, Integral b) => a -> b -> Image -> Image
genericCrop w h = crop (fromIntegral w) (fromIntegral h)

genericCrops :: (Integral a, Integral b) => a -> b -> [Image] -> [Image]
genericCrops = (fmap .) . genericCrop

genericResize :: (Integral a, Integral b) => a -> b -> Image -> Image
genericResize w h = resize (fromIntegral w) (fromIntegral h)

genericResizes :: (Integral a, Integral b) => a -> b -> [Image] -> [Image]
genericResizes = (fmap .) . genericResize

genericTranslate :: (Integral a, Integral b) => a -> b -> Image -> Image
genericTranslate w h = translate (fromIntegral w) (fromIntegral h)

genericTranslates :: (Integral a, Integral b) => a -> b -> [Image] -> [Image]
genericTranslates = (fmap .) . genericTranslate

genericTranslateX :: Integral a => a -> Image -> Image
genericTranslateX = translateX . fromIntegral

genericTranslateXs :: Integral a => a -> [Image] -> [Image]
genericTranslateXs = fmap . genericTranslateX

genericTranslateY :: Integral a => a -> Image -> Image
genericTranslateY = translateY . fromIntegral

genericTranslateYs :: Integral a => a -> [Image] -> [Image]
genericTranslateYs = fmap . genericTranslateY

genericImageWidth :: Num a => Image -> a
genericImageWidth = fromIntegral . imageWidth

genericImageWidths :: Num a => [Image] -> a
genericImageWidths = fromIntegral . maximum . fmap imageWidth

genericImageHeight :: Num a => Image -> a
genericImageHeight = fromIntegral. imageHeight

genericImageHeights :: Num a => [Image] -> a
genericImageHeights = fromIntegral. maximum . fmap imageHeight

genericImageSize :: (Num a, Num b) => Image -> (a,b)
genericImageSize img = (genericImageWidth img, genericImageHeight img)

genericImageSizes :: (Num a, Num b) => [Image] -> (a,b)
genericImageSizes img = (genericImageWidths img, genericImageHeights img)
