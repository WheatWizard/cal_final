{-# LANGUAGE ScopedTypeVariables #-}

module Game.NetFlak.Files (Resource, safeReadResource, loadLevel) where

import Data.Char
import Data.Bifunctor

import System.FilePath
import System.IO.Error

import Paths_netflak

import Game.NetFlak.Types.Level

-- This module replaces some of the functions from System.IO with versions that aren't shite

class Resource a where
  safeReadResource :: FilePath -> IO (Either (String,String) a)

instance Read a => Resource (Level a) where
  safeReadResource = safeReadFile "levels"

safeReadFile :: (Read a, Resource a) => FilePath -> FilePath -> IO (Either (String,String) a)
safeReadFile rdir file = do
  resFile <- getResourceFile rdir file
  resourceData <- safeRead resFile
  case resourceData of
    Right dat -> return $ Right dat
    Left  err -> fmap (first $ (,) err) $ safeRead file
  where safeRead file = catchIOError (parseFileContents <$> readFile file) (return . Left . ioeGetErrorString)

getResourceFile :: FilePath -> FilePath -> IO FilePath
getResourceFile dir file = getDataFileName $ dir </> file

parseFileContents :: forall a. Read a => String -> Either String a
parseFileContents contents =
  case fmap fst $ filter (all isSpace . snd) $ (reads :: ReadS a) contents of
    [parsed] -> Right parsed
    []       -> Left "bad format"
    parses   -> Left $ "ambiguous parse (" ++ show (length parses) ++ " possible parses)"

loadLevel :: Resource a => String -> IO (Either String a)
loadLevel fileName = do
  fileContents <- safeReadResource fileName
  case fileContents of
    Right file -> return $ Right file
    Left (err1, err2) ->
      return $ Left $ "Error loading resource from " ++ fileName ++ ":\n\tResource: " ++ err1 ++ "\n\tnon-Resource: " ++ err2

