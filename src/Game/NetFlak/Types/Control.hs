module Game.NetFlak.Types.Control where

import Game.NetFlak.Types.Vector

data Control
 = Move Vector
 | CastSpell
 | ClearSpell
 | QuitGame
 | ToggleLook
 | ToggleInv
 | ToggleEnemy
 | ToggleMessages
 | ToggleSettings
 | ToggleControls
 | ExitMode
 | UseQuickPotion
 | MenuToggle
 | MenuSelect
 | Menu Integer
 | Crement Integer
 -- TODO expand to more than just Chars
 | UseHotkey Char
 | NoControl
 deriving
   ( Show
   , Eq
   )
