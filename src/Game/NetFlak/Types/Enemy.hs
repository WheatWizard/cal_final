{-# Language TemplateHaskell #-}

module Game.NetFlak.Types.Enemy where

import Lens.Simple

import Game.NetFlak.Types.EnemyMemory
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.PseudoColor

data AIStyle
  = Simple
  | Inactive
  deriving (Show, Read, Eq)

-- It would be nice to have a simply recursive growth
-- However we need these to be readable/showable so for now
-- just geometric and linear will have to do 
data GrowthType
  = NoChange
  | Geometric Integer
  | Linear    Integer
  deriving
    ( Show
    , Read
    , Eq
    )

data Enemy = Enemy
  { _iconChar      :: Char
  , _iconPColor    :: PseudoColorID
  , _name          :: String
  , _enemyHealth   :: Integer
  , _enemyDamage   :: Integer
  , _enemyStack    :: [ Integer ]
  , _enemyGrowth   :: GrowthType
  , _memories      :: EnemyMemory
  , _attackEffects :: [ Potion ]
  , _ai            :: AIStyle
  , _statueHealth  :: Maybe Integer
  }
  deriving
    ( Show
    , Read
    , Eq
    )
makeLenses ''Enemy

