{-# Language TemplateHaskell #-}

module Game.NetFlak.Types.Player where

import Lens.Simple

import Game.NetFlak.Types.Inventory
import Game.NetFlak.Types.RichString

data Player = Player
  { _playerInv    :: Inventory
  , _damageTaken  :: Integer
  , _maxHealth    :: Integer
  , _bloodOnFeet  :: [(RichString, Integer)]
  , _effectStack  :: Integer
  }
  deriving
    ( Show
    , Eq
    )
makeLenses ''Player

