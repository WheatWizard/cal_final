{-# LANGUAGE TemplateHaskell, Rank2Types #-}

module Game.NetFlak.Types.PlayState where

import Game.NetFlak.Types.Level
import Game.NetFlak.Types.RichString
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.World

import Control.Monad.State

import Lens.Simple

data UIState gen a
  = WalkMode
  | LookMode Vector
  | InventoryMode Bool Integer
  | EnemyMode Integer
  | MessagesMode Integer
  | SettingsMode Integer
  | ControlsMode
  | Prompt [RichString] (UIState gen a) [(Char, PlayState gen a)]
  | Quit

-- The transfer over to State transformers is incomplete
data PlayState gen link = PlayState
  { _nextLevel :: link -> StateT gen Maybe (Level link)
  , _curLevelGen :: gen
  , _save :: World link
  , _currentWorld :: World link
  , _playUIState :: UIState gen link
  }
makeLenses ''PlayState
