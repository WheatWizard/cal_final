module Game.NetFlak.Types.SpellComponent where

data SpellComponent
  = UnlimitedSC         ComponentStructure
  | LimitedSC   Integer ComponentStructure
  deriving (Show, Read, Eq)

type ComponentStructure = [String]
