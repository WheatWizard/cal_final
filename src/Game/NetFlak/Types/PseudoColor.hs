module Game.NetFlak.Types.PseudoColor where

data PseudoColorID
  = CyanPID
  | GreenPID
  | BluePID
  | RedPID
  | YellowPID
  | MagentaPID
  | BlackPID
  | DefaultPID
  | GreyPID
  deriving
    ( Show
    , Read
    , Eq
    )

