{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}

module Game.NetFlak.Types.Vector (Vector, manhattan, transpose, mapVec) where

type Vector = (Integer, Integer)

transpose :: Vector -> Vector
transpose = ((,) . snd) <*> fst 

mapVec :: (Integer -> Integer) -> Vector -> Vector
mapVec f (vecA,vecB) = (f vecA,f vecB)

liftPair :: (a -> b) -> (a,a) -> (b,b)
liftPair f = (,) . f . fst <*> f . snd

liftPair2 :: (a -> b -> c) -> (a,a) -> (b,b) -> (c,c)
liftPair2 f (a,b) (c,d) = (f a c, f b d)

instance Num Vector where
  (+)         = liftPair2 (+)
  (-)         = liftPair2 (-)
  (*)         = liftPair2 (*) -- weird but hopefully it won't matter
  abs         = liftPair abs
  signum      = liftPair signum
  fromInteger = (,) <*> id

--TODO Move this to a file
-- Gets the manhattan distance between two points
manhattan :: (Num a) => Vector -> Vector -> a
manhattan (a,b) (a',b') = fromInteger $ abs (a-a') + abs (b-b')
