{-# Language TemplateHaskell #-}

module Game.NetFlak.Types.Inventory where

import Lens.Simple

import Data.Map.Strict (Map)

import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Potion

data Inventory = Inventory
  { _heldComponents :: [SpellComponent] 
  , _heldPotions    :: Map Potion Integer 
  , _quickPotion    :: Maybe Potion
  , _hotbarMap      :: Map Char Integer
  } deriving
    ( Show
    , Eq
    )
makeLenses ''Inventory
