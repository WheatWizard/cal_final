module Game.NetFlak.Types.Potion where

-- TODO make this with Template Haskell
orderedPotions :: [ Potion ]
orderedPotions =
  [ HealPot
  , HPUp
  , Noitop
  , Mirror
  , GrowthPot
  , WitherPot
  , CollatzPot
  , StrengthPot
  , WeakPot
  , KeyPot
  , TimePot
  ]

data Potion
  = HealPot
  | HPUp
  | Noitop
  | Mirror
  | GrowthPot
  | WitherPot
  | CollatzPot
  | StrengthPot
  | WeakPot
  | KeyPot
  | TimePot
  deriving
    ( Show
    , Read
    , Eq
    , Ord
    )
