{-# Language TemplateHaskell #-}

module Game.NetFlak.Types.Settings where

import Graphics.Vty

import Game.NetFlak.Types.Control

import Lens.Simple

data Settings = Settings
  { _maxSpellCycles :: Integer
  -- Sizes for various UI elements
  , _hotbarWidth    :: Integer
  , _spellbarWidth  :: Integer
  , _messageWidth   :: Integer
  , _infobarWidth   :: Integer
  , _enemybarWidth  :: Integer
  , _userControls   :: [((Key, [Modifier]), Control)]
  , _permadeath     :: Bool
  -- Replace Char with (Key, [Modifier])
  , _hotkeys        :: String
  }
  deriving
    ( Show
    , Eq
    )
makeLenses ''Settings
