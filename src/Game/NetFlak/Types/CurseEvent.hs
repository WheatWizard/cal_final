module Game.NetFlak.Types.CurseEvent where

import qualified Graphics.Vty.Input.Events as Vty

type CurseEvent = Vty.Event
