{-# Language TemplateHaskell #-}
module Game.NetFlak.Types.Door where

import Game.NetFlak.Types.Vector

import Lens.Simple

data DoorCondition
 = Always
 | LevelClear
 | Never
 deriving
   ( Show
   , Read
   , Eq
   )

data Door a = Door
  { _isOpen         :: Bool
  , _doorPosition   :: Vector
  , _openCondition  :: DoorCondition
  -- Will always be Never for now
  , _closeCondition :: DoorCondition
  , _destination    :: a
  }
  deriving
    ( Show
    , Read
    , Eq
    )
makeLenses ''Door
