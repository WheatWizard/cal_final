{-# Language TemplateHaskell #-}

module Game.NetFlak.Types.Spell (
    Spell(Spell)
  , ConstructionStep(..)
  , spellContents
  , usedComps
  , recipe
  ) where

import Game.NetFlak.Types.SpellComponent

import Lens.Simple

import Data.Map (Map(..))

data ConstructionStep
 = DoReverse
 | DoMirror
 | AddComponent ComponentStructure
 deriving (Show, Eq)

data Spell = Spell
  { _spellContents :: String
  , _recipe        :: [ ConstructionStep ]
  , _usedComps     :: Map ComponentStructure Integer
  } deriving (Show, Eq)
makeLenses ''Spell
