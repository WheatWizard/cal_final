module Game.NetFlak.Generator.SpellTool
  ((+*+)
  , compose
  )
  where 

-- Seemless Concatenation
(+*+) :: [[a]] -> [[a]] -> [[a]]
[] +*+ b  = b
a  +*+ [] = a
[a] +*+ (b1 : b) = (a ++ b1) : b
(a1 : a) +*+ b   = a1 : a +*+ b

-- Composes the structure of 
compose :: [[a]] -> [[a]] -> [[a]]
compose [] b  = []
compose [a] b = [a]
compose (a1 : a) b = [a1] +*+ b +*+ compose a b

