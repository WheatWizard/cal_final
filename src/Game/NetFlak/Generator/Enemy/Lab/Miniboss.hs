{-# LANGUAGE FlexibleContexts #-}

module Game.NetFlak.Generator.Enemy.Lab.Miniboss
  ( generateLabMiniboss
  ) where

import Control.Monad.Reader (MonadReader, ask)
import Control.Monad.Random (MonadRandom, getRandomR, getRandomRs)

import Game.NetFlak.Generator.Util

import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyMemory
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.Potion

import Lens.Simple

generateLabMiniboss ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m Enemy
generateLabMiniboss = do
  difficulty <- view difficulty <$> ask
  enemyName <- generateMinibossName
  enemyDamage <- getRandomR (0, 1)
  let
    iconChar = case enemyName of
      (x : _) -> x
      _       -> '?'
    upperBound = 2 ^ (2 * div difficulty 5) + 5
    lowerBound = if difficulty < 15
      then 0
      else -upperBound
  enemyHealth <- getRandomR (lowerBound, upperBound)
  enemyStack  <- getRandomRs (lowerBound, upperBound)
  stackHeight <- getRandomR (0, 5)
  effect <- choose
    [ Mirror
    , Noitop
    , GrowthPot
    , WitherPot
    , CollatzPot
    , WeakPot
    ]
  return Enemy
    { _iconChar      = iconChar
    , _iconPColor    = RedPID
    , _name          = enemyName
    , _enemyHealth   = enemyHealth
    , _enemyDamage   = enemyDamage
    , _enemyStack    = take stackHeight enemyStack
    , _enemyGrowth   = NoChange
    , _memories      = NoMovement
    , _ai            = Simple
    , _attackEffects = [effect]
    , _statueHealth  = Nothing
    }

generateMinibossName ::
  ( MonadRandom m
  )
    => m String
generateMinibossName = choose
  [ "Humonculus"
  , "Golem"
  , "Slime Beast"
  , "Chimera"
  ]
