{-# LANGUAGE FlexibleContexts #-}

module Game.NetFlak.Generator.Enemy.Hallway
  ( generateHallwayEnemy
  , generateHallwayHealth
  ) where

import Control.Monad.Reader (MonadReader, ask)
import Control.Monad.Random (MonadRandom, getRandomR, getRandomRs)
import Control.Monad.State (MonadState, get, modify)

import Data.Set
  ( Set
  , notMember
  , lookupMax
  , insert
  )

import Game.NetFlak.Generator.Util

import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyMemory
import Game.NetFlak.Types.PseudoColor

import Lens.Simple

generateHallwayHealth ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  , MonadState  (Set Integer) m
  )
    => Integer -> m Integer
generateHallwayHealth bound = do
  difficulty       <- view difficulty <$> ask
  occupiedHealthes <- get
  let
    -- The max ensures that we always have a unique option not already occupied
    upperBound = case lookupMax occupiedHealthes of
      Just n ->
        max (2 ^ (div difficulty 5) * (div bound 2)) (n + 1)
      Nothing -> 2 ^ (div difficulty 5) * (div bound 2)
    lowerBound = if difficulty < 15
      then 0
      else -(div upperBound 2)
  -- This will always halt because of our unique option not already occupied
  result <- getUntil (flip notMember occupiedHealthes) $
    getRandomR (lowerBound, upperBound)

  -- Add the health to the occupied healthes
  modify $ insert result

  return result
  

generateHallwayEnemy ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  , MonadState  (Set Integer) m
  )
    => Integer -> m Enemy
generateHallwayEnemy bound = do
  enemyName <- generateName
  enemyDamage <- getRandomR (1, 12)
  enemyHealth <- generateHallwayHealth bound
  let
    iconChar = case enemyName of
      (x : _) -> x
      _       -> '?'
  return Enemy
    { _iconChar      = iconChar
    , _iconPColor    = RedPID
    , _name          = enemyName
    , _enemyHealth   = enemyHealth
    , _enemyDamage   = enemyDamage
    , _enemyStack    = []
    , _enemyGrowth   = NoChange
    , _memories      = NoMovement
    , _ai            = Simple
    , _attackEffects = []
    , _statueHealth  = Nothing
    }

generateName ::
  ( MonadRandom m
  )
    => m String
generateName = choose
  [ "Critter"
  , "Fiend"
  , "Varmint"
  , "Minion"
  , "Acolyte"
  ]
