{-# Language FlexibleContexts #-}

module Game.NetFlak.Generator.Enemy.StatueGarden.Keeper (generateKeeper) where

import Control.Monad.Random.Class
import Control.Monad.Reader.Class

import Game.NetFlak.Generator.Util

import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyMemory
import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.PseudoColor

import Lens.Simple

generateKeeper ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m Enemy
generateKeeper = do
  difficulty <- view difficulty <$> ask
  enemyDamage <- getRandomR (1, 12) 
  let
    upperBound = 2 ^ (div difficulty 5) + 3
    lowerBound = 0
  enemyHealth <- getRandomR (lowerBound, upperBound) 
  stackItem   <- getRandomR (0, enemyHealth)
  return Enemy
    { _iconChar      = 'K'
    , _iconPColor    = RedPID
    , _name          = "Keeper"
    , _enemyHealth   = enemyHealth
    , _enemyDamage   = enemyDamage
    , _enemyStack    = [stackItem]
    , _enemyGrowth   = NoChange
    , _memories      = NoMovement
    , _ai            = Simple
    , _attackEffects = []
    , _statueHealth  = Nothing
    }
