{-# LANGUAGE FlexibleContexts #-}

module Game.NetFlak.Generator.Enemy.Miniboss
  ( generateMiniboss
  ) where

import Control.Monad.Reader (MonadReader, ask)
import Control.Monad.Random (MonadRandom, getRandomR, getRandomRs)

import Game.NetFlak.Generator.Util

import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyMemory
import Game.NetFlak.Types.PseudoColor

import Lens.Simple

generateMiniboss ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m Enemy
generateMiniboss = do
  difficulty <- view difficulty <$> ask
  enemyName <- generateMinibossName
  enemyDamage <- getRandomR (1, 12)
  let
    iconChar = case enemyName of
      (x : _) -> x
      _       -> '?'
    upperBound = 2 ^ (2 * div difficulty 5) + 3
    lowerBound = if difficulty < 15
      then 0
      else -upperBound
  enemyHealth <- getRandomR (lowerBound, upperBound)
  enemyStack  <- getRandomRs (lowerBound, upperBound)
  stackHeight <- getRandomR (0, 5)
  return Enemy
    { _iconChar      = iconChar
    , _iconPColor    = RedPID
    , _name          = enemyName
    , _enemyHealth   = enemyHealth
    , _enemyDamage   = enemyDamage
    , _enemyStack    = take stackHeight enemyStack
    , _enemyGrowth   = NoChange
    , _memories      = NoMovement
    , _ai            = Simple
    , _attackEffects = []
    , _statueHealth  = Nothing
    }

generateMinibossName ::
  ( MonadRandom m
  )
    => m String
generateMinibossName = choose
  [ "Beast"
  , "Monstrosity"
  , "Horror"
  , "Atrocity"
  , "Abomination"
  , "Glutton"
  , "Monster"
  ]
