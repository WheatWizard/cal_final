{-# LANGUAGE FlexibleContexts #-}

module Game.NetFlak.Generator.Level
  ( generateLevel
  ) where

import Control.Monad.Random
import Control.Monad.Reader (MonadReader, ask)

import Game.NetFlak.Generator.Level.Cage
import Game.NetFlak.Generator.Level.Hallway
import Game.NetFlak.Generator.Level.Lab.Cage
import Game.NetFlak.Generator.Level.Lab.Hallway
import Game.NetFlak.Generator.Level.StatueGarden.Hallway
import Game.NetFlak.Generator.Level.StatueGarden.TrophyRoom
import Game.NetFlak.Generator.Util (chooseM)

import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.SpecialArea

import Lens.Simple

generateLevel ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m EndlessLevel
generateLevel = do
  specialArea <- view specialArea <$> ask
  case specialArea of
    Normal ->
      chooseM
        [ generateHallway
        , generateCage
        ]
    Alchemist ->
      chooseM
        [ generateLabHallway
        , generateLabCage
        ]
    Statue ->
      chooseM
        [ generateStatueGardenHallway
        , generateTrophyRoom
        ]
