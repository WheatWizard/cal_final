{-# LANGUAGE FlexibleContexts #-}

module Game.NetFlak.Generator.Potion
  ( generatePotion
  ) where

import Control.Monad.Reader (MonadReader)
import Control.Monad.Random (MonadRandom)

import Game.NetFlak.Generator.Util

import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Potion


generatePotion ::
  ( MonadRandom m
  )
    => m Potion
generatePotion = choose
  [ GrowthPot
  , WitherPot
  , CollatzPot
  , HPUp
  , HealPot
  , StrengthPot
  , KeyPot
  , TimePot
  ]
