{-# Language FlexibleContexts, NoMonomorphismRestriction #-}

module Game.NetFlak.Generator.Level.StatueGarden.Hallway
  ( generateStatueGardenHallway
  ) where

import Control.Monad (liftM2)
import Control.Monad.Random.Class hiding (fromList)
import Control.Monad.Reader.Class
import Control.Monad.State.Class
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State.Lazy hiding (modify, get)

import Data.Set (Set, fromList, toList, insert)

import Lens.Simple

import Game.NetFlak.Generator.Level.Hallway (generateHallway)
import Game.NetFlak.Generator.Enemy.Hallway (generateHallwayEnemy, generateHallwayHealth)
import Game.NetFlak.Generator.Util (choose, chooseM)

import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.SpecialArea

generateHallwayStatue ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  , MonadState  (Set Integer) m
  )
    => Integer -> m Enemy
generateHallwayStatue bound = do
  occupiedHealthes <- get
  -- Get a normal hallway enemy one quarter the difficulty
  enemy <- local (over difficulty (`div` 4) . set specialArea Normal) $
    generateHallwayEnemy bound
  -- Make the enemy a statue
  (flip (set statueHealth) enemy . Just) <$>
    -- Statue may be sampled from the health of a live enemy
    chooseM
      [ choose (toList occupiedHealthes)
      , generateHallwayHealth bound
      ]

addHallwayStatue ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  , MonadState  (Set Integer) m
  )
    => Integer -> (StateT EndlessLevel m) ()
addHallwayStatue bound = do
  lvl <- get
  -- Get two enemies
  e1 <- lift $ generateHallwayStatue bound
  e2 <- lift $ generateHallwayStatue bound
  -- Add the new enemies to the existing level
  modify $
    over enemies (((0, bound), e1) :) .
      over enemies (((view (levelSize . _1) lvl - 1, bound), e2) :)

generateStatueGardenHallway ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m EndlessLevel
generateStatueGardenHallway = do
  startingHall <- over startMessages ([(GreyPID, "Statue Garden")] :) <$>
    local (set specialArea Normal) generateHallway
  let
    healthSet :: Set Integer 
    healthSet = fromList $ view (_2 . enemyHealth) <$> view enemies startingHall
  flip evalStateT healthSet $ flip execStateT startingHall $ sequence_ $ do
    n <- [2, 5 .. view (levelSize . _2) startingHall - 1]
    return $ addHallwayStatue n
