{-# Language TemplateHaskell, FlexibleContexts #-}

module Game.NetFlak.Generator.Level.Lab.Cage (generateLabCage) where

import Control.Monad.Random
import Control.Monad.Reader.Class
import Control.Monad.State.Lazy

import Game.NetFlak.Generator.Enemy.Lab.Miniboss
import Game.NetFlak.Generator.Potion
import Game.NetFlak.Generator.SpellComponent
import Game.NetFlak.Generator.Util

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.Vector

import Game.NetFlak.Level (inside)

import Lens.Simple

data CageState = CageState
  { _levelWidth  :: (Integer, Integer)
  , _levelHeight :: (Integer, Integer)
  , _potions     :: [(Vector, Potion)]
  , _components  :: [(Vector, SpellComponent)]
  }
makeLenses ''CageState

generateLocation ::
  ( MonadRandom m
  , MonadState CageState m
  )
    => m Vector
generateLocation = do
  cageState <- get

  newA <- getRandomR $ widen $ view levelWidth cageState
  newB <- getRandomR $ widen $ view levelHeight cageState
  
  modify $ over levelWidth  (stretch newA)
  modify $ over levelHeight (stretch newB)

  return (newA, newB)
  where
    widen (a, b) = (a - 3, b + 3)
    stretch c (a, b) = (min c a, max c b)

addNothing ::
  ( MonadRandom m
  , MonadState CageState m
  )
    => m ()
addNothing = do
  _ <- generateLocation
  return ()

-- Can we get rid of the StateT in the type signature?
addNewPotion ::
  ( MonadRandom m
  )
    => m Potion -> StateT CageState m ()
addNewPotion generatePotion = do
  newPotion <- lift generatePotion
  loc <- generateLocation

  modify $ removeItemsAt loc
  modify $ over potions ((loc, newPotion) :)
  

defState :: CageState
defState = CageState
  { _levelWidth  = (0, 3)
  , _levelHeight = (0, 3)
  , _potions    = []
  , _components = []
  }

removeItemsAt :: Vector -> CageState -> CageState
removeItemsAt loc
  = over potions    (filter ((/= loc) . fst)) .
    over components (filter ((/= loc) . fst))

recenter :: CageState -> CageState
recenter cageState =
  over levelWidth  ((,) 0 . (1 +) . uncurry subtract) $
    over levelHeight ((,) 0 . (1 +) . uncurry subtract) $
      over (potions . mapped . _1) recenterVec $
        over (components . mapped . _1) recenterVec $
          cageState
  where
    recenterVec :: Vector -> Vector
    recenterVec =
      over _1 (subtract $ view (levelWidth . _1) cageState) .
        over _2 (subtract $ view (levelHeight . _1) cageState)
   

fillState ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  , MonadState  CageState m
  )
    => m EndlessLevel
fillState = do
  cageState <- recenter <$> get
  let 
    sizeA = view (levelWidth  . _2) cageState
    sizeB = view (levelHeight . _2) cageState
    getLocation = do
      a <- getRandomR (0, sizeA - 1)
      b <- getRandomR (0, sizeB - 1)
      return (a,b)

  playerLocation <- getLocation

  enemyLocation <- 
    -- Enemy should not be where the player is
    getUntil (/= playerLocation) $
      -- Enemy should not be out of bounds
      getUntil (flip inside (sizeA, sizeB)) $ do
        totalDistance <- enemyDistance
        distA <- getRandomR (0, totalDistance)
        let
          distB = totalDistance - distA
        (playerLocation +) <$> choose
          [ ( distA,  distB)
          , (-distA,  distB)
          , ( distA, -distB)
          , (-distA, -distB)
          ]

  enemy <- generateLabMiniboss

  exitDoorLoc <- chooseM
    [      (,) (-1)  <$> getRandomR (0, sizeB - 1)
    , flip (,) (-1)  <$> getRandomR (0, sizeA - 1)
    ,      (,) sizeA <$> getRandomR (0, sizeB - 1)
    , flip (,) sizeB <$> getRandomR (0, sizeA - 1)
    ]

  let
    newState = removeItemsAt playerLocation $ removeItemsAt enemyLocation cageState

  return $ Level 
    { _levelSize       = (sizeA, sizeB)
    , _playerLocation  = playerLocation
    , _floorComponents = view components newState
    , _floorPotions    = view potions newState
    , _enemies         = [(enemyLocation, enemy)]
    , _exitDoors       = [ Door
      { _doorPosition   = exitDoorLoc
      , _isOpen         = False
      , _openCondition  = LevelClear
      , _closeCondition = Never
      , _destination    = Nothing
      } ]
    , _startMessages = [[(RedPID, "Alchemist's Lab")]]
    }

-- A Cage is an area where you fight a single tough enemy
generateLabCage ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m EndlessLevel
generateLabCage =
  flip evalStateT defState $ do
    -- Add items
    amount <- getRandomR (3, 7)
    sequence_ $ replicate amount $
      join $ uniform
        [ addNewPotion generatePotion
        , addNothing
        ]
    fillState
