{-# Language FlexibleContexts, TemplateHaskell #-}
module Game.NetFlak.Generator.Level.Lab.Hallway
  ( generateLabHallway
  ) where


import Control.Monad.Random
import Control.Monad.State.Lazy
import Control.Monad.Reader (MonadReader, ask)

import Data.Set (Set, insert, empty)

import Game.NetFlak.Generator.Enemy.Lab.Hallway
import Game.NetFlak.Generator.SpellComponent
import Game.NetFlak.Generator.Potion
import Game.NetFlak.Generator.Util

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.Vector

import Lens.Simple

data HallwayState = HallwayState
  { _bound            :: Integer
  -- Width should be read only
  , _width            :: Integer
  , _mobs             :: [(Vector, Enemy)]
  , _components       :: [(Vector, SpellComponent)]
  , _potions          :: [(Vector, Potion)]
  , _occupiedHealthes :: Set Integer
  }
makeLenses ''HallwayState

getCenterLine ::
  ( MonadState HallwayState m
  )
    => m Integer
getCenterLine = flip div 2 <$> gets (view width)

setNewBoundBy ::
  ( MonadState HallwayState m
  )
    => Integer -> m ()
setNewBoundBy increment = do
  modify $ over bound (+ increment)

setNewBound ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  , MonadState  HallwayState m
  )
    => m ()
setNewBound = enemyDistance >>= setNewBoundBy

addEnemy ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  , MonadState  HallwayState m
  )
    => m ()
addEnemy = do
  setNewBound
  oHealthes <- gets $ view occupiedHealthes
  hallBound <- gets $ view bound
  hallCenter <- getCenterLine
  newEnemy <- generateLabHallwayEnemy hallBound oHealthes
  modify $ over mobs (((hallCenter, hallBound - 1), newEnemy) :)
  modify $ over occupiedHealthes $ insert $ view enemyHealth newEnemy
  
addPotion ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  , MonadState  HallwayState m
  )
    => m ()
addPotion = do
  setNewBound
  newPotion <- generatePotion
  hallBound <- gets $ view bound
  hallCenter <- getCenterLine
  modify $ over potions (((hallCenter, hallBound - 1), newPotion) :)

defState :: HallwayState
defState = HallwayState
  { _bound            = 3
  , _width            = 3
  , _components       = []
  , _potions          = []
  , _mobs             = []
  , _occupiedHealthes = empty
  }

fillHallState ::
  ( MonadState HallwayState m
  )
    => m EndlessLevel
fillHallState = do
  hallBound      <- gets $ view bound
  hallWidth      <- gets $ view width
  hallCenter     <- getCenterLine
  hallComponents <- gets $ view components
  hallPotions    <- gets $ view potions
  hallEnemies    <- gets $ view mobs
  return $ Level
    { _levelSize       = (hallWidth, hallBound)
    , _playerLocation  = (hallCenter, 1)
    , _floorComponents = hallComponents
    , _floorPotions    = hallPotions
    , _enemies         = hallEnemies
    , _exitDoors       = [ Door
      { _doorPosition   = (hallCenter, hallBound)
      , _isOpen         = False
      , _openCondition  = LevelClear
      , _closeCondition = Never
      , _destination    = Nothing
      } ]
    -- TODO actual start message
    , _startMessages   =
      [
        [ (RedPID, "Alchemist's Lab")
        ]
      ]
    }

-- Hallways are long rooms with lots of enemies
generateLabHallway ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m EndlessLevel
generateLabHallway =
  flip evalStateT defState $ do
    -- Get and set Level width
    -- TODO variable width
    hallWidth <- getRandomR (3, 3)
    modify $ set width hallWidth
    
    -- Add enemies
    amount <- getRandomR (1, 4)
    sequence_ $ replicate amount $ do
      itemAmount <- getRandomR (0, 3) >>= curry getRandomR 0
      -- Add items
      sequence_ $ replicate itemAmount $ addPotion
      addEnemy
    fillHallState

