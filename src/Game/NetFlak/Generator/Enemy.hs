{-# LANGUAGE FlexibleContexts #-}

module Game.NetFlak.Generator.Enemy
  ( generateEnemy 
  ) where

import Control.Monad.Reader (MonadReader, ask)
import Control.Monad.Random (MonadRandom, getRandomR, getRandomRs)


import Game.NetFlak.Generator.Util

import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyMemory
import Game.NetFlak.Types.PseudoColor

import Lens.Simple

generateEnemy ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m Enemy
generateEnemy = do
  difficulty <- view difficulty <$> ask
  enemyName <- generateName
  enemyDamage <- getRandomR (1, 12)
  let
    iconChar = case enemyName of
      (x : _) -> x
      _       -> '?'
    upperBound = 2 ^ (div difficulty 5) + 3
    lowerBound = if difficulty < 15
      then 0
      else -upperBound
  enemyHealth <- getRandomR (lowerBound, upperBound)
  return Enemy
    { _iconChar      = 'T'
    , _iconPColor    = RedPID
    , _name          = enemyName
    , _enemyHealth   = enemyHealth
    , _enemyDamage   = enemyDamage
    , _enemyStack    = []
    , _enemyGrowth   = NoChange
    , _memories      = NoMovement
    , _ai            = Simple
    , _attackEffects = []
    , _statueHealth  = Nothing
    }

generateName ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m String
generateName = choose
  [ "Critter"
  , "Fiend"
  , "Varmint"
  , "Minion"
  , "Acolyte"
  ]
