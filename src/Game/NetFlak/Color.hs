module Game.NetFlak.Color where

import Graphics.Vty

import Game.NetFlak.Types.PseudoColor

getColor :: PseudoColorID -> Color
getColor CyanPID    = cyan
getColor GreenPID   = green
getColor BluePID    = blue
getColor RedPID     = red
getColor YellowPID  = yellow
getColor MagentaPID = magenta
getColor BlackPID   = black
getColor DefaultPID = brightWhite
getColor GreyPID    = brightBlack
