{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies, ScopedTypeVariables, LambdaCase #-}

module Game.NetFlak.ProgramMode
  ( Quittable
  , quitEvent
  , ProgramMode
  , uiState
  , renderData
  , nextState
  , render
  , handleDisplay
  ) where

import Control.Monad.Random

import Game.NetFlak.Types.CurseEvent
import Game.NetFlak.Types.Vector

import Graphics.Vty hiding (Event)

import Lens.Simple

import Reactive.Banana
import Reactive.Banana.Frameworks

class Quittable state where
  quitEvent :: state -> Bool

class Quittable uiState => ProgramMode progState uiState renderData | progState -> uiState renderData, renderData uiState -> progState where
  uiState :: Functor f => (uiState -> f uiState) -> progState -> f progState
  renderData :: Functor f => (renderData -> f renderData) -> progState -> f progState
  nextState :: RandomGen g => (Key, [Modifier]) -> (progState, g) -> (progState, g)
  render :: Vector -> (uiState, renderData) -> Picture

-- A event stream of key events
-- (leaves out events like clicking, resizing etc.)
filterKeys :: Event CurseEvent -> Event (Key, [Modifier])
filterKeys input = filterJust $ (<$> input) $ \case
      EvKey key mods -> Just (key, mods)
      _              -> Nothing

-- Extracts data needed for rendering from a program state
getCompleteRenderData :: ProgramMode progState uiState renderData => progState -> (uiState, renderData)
getCompleteRenderData gs = (view uiState gs, view renderData gs)

handleDisplay ::
  ProgramMode progState uiState renderData
    => progState -> Event CurseEvent -> MomentIO (Behavior Picture, Event (Vty -> IO ()))
handleDisplay initialState input = do
  -- Set up the random generator
  randGen <- liftIO getStdGen
  -- Create a screenSize event stream
  screenSize <- stepper undefined $ filterJust $ (<$>input) $ \case
    EvResize a b -> Just (toInteger a, toInteger b)
    _            -> Nothing
  -- Creates the steam of states
  stateEvents      <- accumE (initialState, randGen) $ nextState <$> filterKeys input
  let renderEvents =  getCompleteRenderData . fst <$> stateEvents
  stateBehavior    <- stepper (getCompleteRenderData initialState) renderEvents

  return 
    ( fmap render screenSize <*> stateBehavior
    , shutdown <$ filterE (quitEvent . view (_1 . uiState)) stateEvents
    )
