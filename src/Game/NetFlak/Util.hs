module Game.NetFlak.Util where

import Data.List (genericLength, genericTake, genericIndex)

-- For strings longer than the given length, shortens them and indicates the shortening with a trailing "..."
-- If the given length is less than three the entire string will be converted into the given length number of dots
trim :: Integral a => a -> String -> String
trim len str
  | genericLength str <= len = str
  | genericLength str >  len = genericTake len $ genericTake (len - 3) str ++ "..."

strongFoldl :: ([a] -> b -> a) -> [b] -> [a] 
strongFoldl f a = reverse $ strongFoldl' [] f a
  where strongFoldl' x _ [] = x
        strongFoldl' x f (a:b) = strongFoldl' (f x a:x) f b

charToNum :: Integral a => Char -> a
charToNum n = fromIntegral $ fromEnum n - fromEnum '0'

mirror :: String -> String
mirror = map subMirror . reverse
  where
    subMirror :: Char -> Char
    subMirror '(' = ')'
    subMirror ')' = '('
    subMirror '<' = '>'
    subMirror '>' = '<'
    subMirror '[' = ']'
    subMirror ']' = '['
    subMirror '{' = '}'
    subMirror '}' = '{'
    subMirror  x  =  x

maybeIndex :: Integral n => [a] -> n -> Maybe a
maybeIndex xs i
 | i >= 0 && i < genericLength xs = Just $ genericIndex xs i
 | otherwise = Nothing

isBalanced :: String -> Bool
isBalanced = (== ([], [], [])) . unbalanced

-- Unbalanced takes a string and finds all the unbalanced braces.
-- It returns a triple of lists of indices.
-- The first list is braces that are never closed.
-- The second is braces that matched incorrectly.
-- The third is braces that are never opened.
unbalanced :: (Num a, Enum a) => String -> ([a], [a], [a])
unbalanced s = go [] [id] [] $ zip s [0 ..]
  where
    -- mms is a list of difference lists used to build the sorted list of the indices of mismatches
    -- An important invariant: length mms - length op is constant, for normal inputs, it should be 1
    go :: [(Char, a)] -> [[a] -> [a]] -> [a] -> [(Char, a)] -> ([a], [a], [a])
    go op             mms           cl [] = (reverse $ fmap snd op, foldl (flip ($)) [] mms, reverse cl)
    go op             mms           cl (x@(name, _):xs)
      | elem name "([{<"                        = go (x:op) (id:mms)                      cl         xs
    go ((open, _):op) (mm1:mm2:mms) cl ((close, _):xs)
      | elem [open,close] ["()","[]","{}","<>"] = go op     (mm2 . mm1:mms)               cl         xs
    go []             [mm]          cl ((name, index):xs)
      | elem name ")]}>"                        = go []     [mm]                          (index:cl) xs
    go op             mms           cl ((name, index):xs)
      | notElem name "()[]{}<>"                 = go op     mms                           cl         xs
    go ((_, y):op)    (mm1:mm2:mms) cl ((_, x):xs)
                                                = go op     (mm2 . (y:) . mm1 . (x:):mms) cl         xs

sanitizeChar :: Char -> Char
sanitizeChar c
 | show c == '\'' : c : "'" = c
 | otherwise = '?'

-- Sanitizes strings for printing to prevent strange behavior for control characters
sanitize :: String -> String
sanitize s = case show s of
  ('"' : s') -> case reverse s' of
    ('"' : s'') -> reverse s''
  -- These should never happen
    _ -> ""
  _ -> ""
