{-# Language NoMonomorphismRestriction #-}
module Game.NetFlak.WindowMonad where

import Control.Arrow

import Control.Monad.State.Lazy

import Lens.Simple

import Game.NetFlak.GenericImage
import Game.NetFlak.Window

import Game.NetFlak.Types.Vector

import Graphics.Vty

type WindowMonad = State ([[Image] -> [Image]],Vector)

getScreenSize :: WindowMonad Vector
getScreenSize = snd <$> get

consume :: Integral a => Side -> a -> WindowMonad [Image] -> WindowMonad ()
consume side size wm = do
  (ops, totalSize) <- get
  let
    modSide = case side of
      West  -> _1
      East  -> _1
      North -> _2
      South -> _2
      
    properSize = min (view modSide totalSize) $ max 0 $ toInteger size

    newWindowSize = set modSide properSize totalSize

    -- Extract the images from the windowMonad
    imgs  = extractImages newWindowSize wm

    newOp = case side of
      East  -> flip (++) $
        genericTranslateXs (fst totalSize - properSize) imgs
      South -> flip (++) $
        genericTranslateYs (snd totalSize - properSize) imgs
      _     -> (imgs ++) . uncurry genericTranslates
        (set modSide properSize (0, 0))

  put (newOp : ops, over modSide (subtract properSize) totalSize)

consumeRight, consumeLeft, consumeTop, consumeBottom ::
  Integral a => a -> WindowMonad [Image] -> WindowMonad ()
consumeRight  = consume East
consumeLeft   = consume West
consumeTop    = consume North
consumeBottom = consume South


setPicture :: Window -> WindowMonad [Image]
setPicture win = do
  (ops,size) <- get
  let imgs = constructWindow size win
  return $ foldl (flip ($)) imgs ops

extractImages :: Vector -> WindowMonad [Image] -> [Image]
extractImages size builder = evalState builder ([], size)
