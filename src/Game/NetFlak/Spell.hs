module Game.NetFlak.Spell where

import Prelude hiding (lookup)

import Control.Arrow (first, second)

import Data.List (genericLength, genericIndex, partition, genericDrop, genericTake)
import Data.Map (lookup, insertWith, fromList, empty)

import Lens.Simple

import Game.NetFlak.Items
import Game.NetFlak.Logs
import Game.NetFlak.Util
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyAction
import Game.NetFlak.Types.Inventory
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.LogItem
import Game.NetFlak.Types.Settings
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.World

doDeletes :: String -> String
doDeletes = ([]#)
  where
    as # ('X' : bs) = drop 1 as # bs
    -- Y cannot delete Y
    ('Y' : as) # ('Y' : bs) = ('Y' : 'Y' : as) # bs
    ('Y' : as) # (_ : bs) = as # bs
    as # (b : bs)   = (b : as) # bs
    as # []         = reverse as

useSpellAtIndex :: Integral i => i -> World a -> World a
useSpellAtIndex index world
  | null comp   = world
  | [c] <- comp = over currentSpell (addComponent c) world
  where
    comp = genericDrop index $ genericTake (index + 1) $ view (player . playerInv . heldComponents) world

useComponent :: SpellComponent -> World a -> World a
useComponent comp world = case canUse comp world of
 Nothing  -> over currentSpell (addComponent comp) world
 Just log -> over rawLog (log :) world

-- Could this be replaced with a fold
fillWilds :: ComponentStructure -> String -> String
fillWilds []      s = ""
fillWilds [ a ]   s = a
fillWilds (a : b) s = a ++ s ++ fillWilds b s

addComponent :: SpellComponent -> Spell -> Spell
addComponent (UnlimitedSC structure) oldSpell = addConstructionStep (AddComponent structure) oldSpell
addComponent (LimitedSC _ structure) oldSpell =
  over usedComps (insertWith (+) structure 1) $ addConstructionStep (AddComponent structure) oldSpell

addConstructionStep :: ConstructionStep -> Spell -> Spell
addConstructionStep step = 
  over spellContents (performConstructionStep step) . over recipe (++ [step])

performConstructionStep :: ConstructionStep -> String -> String
performConstructionStep (AddComponent structure) = doDeletes . fillWilds structure
performConstructionStep DoMirror  = mirror
performConstructionStep DoReverse = reverse

partitionCasualties ::
  Spell -> Integer -> [(Vector, Enemy)] -> Either LogItem (([(Vector, Enemy)], [(Vector, Enemy)]), [LogItem])
partitionCasualties spell maxCycles enemies =
  -- We cache the value of an empty stack since it is the most common
  go (attemptSpell spell [] maxCycles) spell maxCycles enemies
  where
   go ::
     Either LogItem Integer -> Spell -> Integer -> [(Vector, Enemy)] -> Either LogItem (([(Vector, Enemy)], [(Vector, Enemy)]), [LogItem])
   go cachedVal spell maxCycles [] = Right (([], []), [ SpellCast spell ])
   go cachedVal spell maxCycles (ePair@(loc, enemy) : enemies) =
     case view statueHealth enemy of
       Just health ->
         case cachedVal of
           Left log  -> Left log
           Right val
             | val == health ->
               (over _2 (StatueAwakens enemy :) . over (_1 . _2) (set (_2 . statueHealth) Nothing ePair :))
                 <$> go cachedVal spell maxCycles enemies
             | otherwise ->
               over (_1 . _2) (ePair :) <$> go cachedVal spell maxCycles enemies
       Nothing ->
         case
           case view enemyStack enemy of
             []    -> cachedVal
             stack -> attemptSpell spell stack maxCycles
           of
             Left log -> Left log
             Right val
               | val == view enemyHealth enemy ->
                 (over _2 (EnemyDeath PlayerKill enemy :) . over (_1 . _1) (ePair :))
                   <$> go cachedVal spell maxCycles enemies
               | otherwise ->
                 over (_1 . _2) (ePair :) <$> go cachedVal spell maxCycles enemies

emptySpell :: Spell
emptySpell = Spell "" [] empty

clearSpell :: World a -> World a
clearSpell = set currentSpell emptySpell
