module Game.NetFlak.Splittable where

import qualified Data.List as List

class Splittable a where
  genericSplitAt :: Integral i => i -> a -> (a, a)
  genericSplitAt = ((<*>) . ((,) .) . genericTake) <*> genericDrop
  genericTake    :: Integral i => i -> a -> a
  genericTake = (fst .) . genericSplitAt
  genericDrop    :: Integral i => i -> a -> a
  genericDrop = (snd .) . genericSplitAt

instance Splittable [a] where
  genericSplitAt = List.genericSplitAt

