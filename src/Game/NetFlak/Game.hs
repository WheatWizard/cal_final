module Game.NetFlak.Game (runFRP, runRenderTest, runBuilder, runEndless) where

import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.State
import Control.Monad.Trans.Reader (runReaderT)
import Control.Monad.Trans.State (StateT)
import Control.Monad.Random hiding (fromList)

import Data.Char (ord, chr)
import Data.Either
import Data.List (genericLength)
import Data.Map (fromList, toList)
import Data.Map.Strict (empty)
import Data.Maybe (listToMaybe)

import Game.NetFlak.Color
import Game.NetFlak.Files
import Game.NetFlak.ProgramMode
import Game.NetFlak.Items
import Game.NetFlak.Generator.Level
import Game.NetFlak.Generator.Util
import Game.NetFlak.Render.Play
import Game.NetFlak.Render.Build
import Game.NetFlak.UI
import Game.NetFlak.World

import Game.NetFlak.StateTransfer.Play
import Game.NetFlak.StateTransfer.Build

import Game.NetFlak.Types.Control
import Game.NetFlak.Types.CurseEvent
import Game.NetFlak.Types.Door
import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Inventory
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.PlayState
import Game.NetFlak.Types.BuildState
import Game.NetFlak.Types.Settings
import Game.NetFlak.Types.SpecialArea
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.World

import Graphics.Vty hiding (Event)

import Lens.Simple

import Reactive.Banana hiding (empty)
import Reactive.Banana.Frameworks

import System.Random

-- Tohu wa bohu
emptyLevel = Level {
    _levelSize       = (10, 10)
  , _playerLocation  = (0, 0)
  , _floorComponents = []
  , _floorPotions   = []
  , _enemies         = []
  , _exitDoors       = [ Door
    { _doorPosition   = (0, -1)
    , _isOpen         = False
    , _openCondition  = LevelClear
    , _closeCondition = Never
    , _destination    = ()
    } ]
  , _startMessages   = []
  }

defControls = [
    ((KChar 'w' , []), Move (-1, 0))
  , ((KChar 's' , []), Move ( 1,  0))
  , ((KChar 'a' , []), Move ( 0, -1))
  , ((KChar 'd' , []), Move ( 0,  1))
  , ((KChar '.' , []), Move ( 0,  0))
  , ((KChar 'q' , []), QuitGame)
  , ((KChar 'l' , []), ToggleLook)
  , ((KChar 'i' , []), ToggleInv)
  , ((KChar 'o' , []), ToggleEnemy)
  , ((KChar 'm' , []), ToggleMessages)
  , ((KChar 'p' , []), ToggleSettings)
  , ((KChar 'k' , []), ToggleControls)
  , ((KChar 'e' , []), CastSpell)
  , ((KChar 'c' , []), ClearSpell)
  , ((KChar '-' , []), Menu (-1))
  , ((KChar '=' , []), Menu 1)
  , ((KChar '\t', []), MenuToggle)
  , ((KChar 'U' , []), UseQuickPotion)
  , ((KChar '[' , []), Crement (-1))
  , ((KChar ']' , []), Crement (1))
  , ((KEnter    , []), MenuSelect)
  , ((KEsc      , []), ExitMode)
  ]

startSettings = Settings {
      _maxSpellCycles = 5000
    , _hotbarWidth    = 20
    , _enemybarWidth  = 20
    , _spellbarWidth  = 5
    , _messageWidth   = 10
    , _infobarWidth   = 10
    , _hotkeys        = "0123456789"
    , _userControls   = defControls
    , _permadeath     = False
    }

startHotbarMap = fromList $ (\x -> (chr $ ord '0' + x, toInteger x)) <$> [0..9]

startPlayer = Player
    { _playerInv    = Inventory {
        _heldComponents = []
      , _heldPotions   = empty
      , _quickPotion    = Nothing
      , _hotbarMap      = startHotbarMap
      }
    , _damageTaken  = 0
    , _maxHealth    = 10
    , _bloodOnFeet  = []
    , _effectStack  = 0
    }

startEndlessPlayer = Player
    { _playerInv    = Inventory {
        _heldComponents = 
        [ UnlimitedSC ["", "()"]
        , UnlimitedSC ["", "{}"]
        ]
      , _heldPotions = empty
      , _quickPotion = Nothing
      , _hotbarMap = startHotbarMap
      }
    , _damageTaken  = 0
    , _maxHealth    = 10
    , _bloodOnFeet  = []
    , _effectStack  = 0
    }

-- A more general version of displayBounds
-- TODO move this to a more appropriate location
genericDisplayBounds :: (Num a, Num b) => Output -> IO (a, b)
genericDisplayBounds output = do
  (a, b) <- displayBounds output
  return (fromIntegral a, fromIntegral b)

runFRP :: [String] -> IO ()
runFRP levelFiles = do
  eitherLevels <- mapM loadLevel levelFiles :: IO [ Either String PuzzleLevel]
  case lefts eitherLevels of
    [] ->
      case rights eitherLevels of
        (firstLevel : otherLevels) -> do
          vty <- standardIOConfig >>= mkVty
          let
            initWorld :: World ()
            initWorld = buildWorld firstLevel startPlayer startSettings
            popLevel :: () -> StateT [PuzzleLevel] Maybe PuzzleLevel
            popLevel _ = do
              ls <- get
              -- Remove the head level (if one exists)
              put $ tail ls
              -- Return maybe the head level
              lift $ listToMaybe ls
          runDisplay vty $ handleDisplay $ PlayState popLevel otherLevels initWorld initWorld WalkMode
        -- TODO This should never happen but should be implemented
        [] -> undefined
    errs -> putStrLn $ unlines errs

nextEnvironment ::
  ( MonadRandom m
  )
    => EnvironmentVariables -> m EnvironmentVariables
nextEnvironment evs = do
  newArea <- case mod (view difficulty evs) 5 of
    0 -> choose
      [ Normal
      , Alchemist
      , Statue
      ]
    _ -> return $ view specialArea evs
  return $ over difficulty (+ 1) $ set specialArea newArea evs
  

runEndless :: IO ()
runEndless = do
  vty <- mkVty =<< standardIOConfig
  let
    startEVs = EnvironmentVariables 0 Normal
  -- Make the first level
  firstLevel <- runReaderT generateLevel startEVs
  let
    -- Build the first world from the first level
    initWorld = buildWorld firstLevel startEndlessPlayer startSettings
    genNewLevel :: Maybe SpecialArea -> StateT (EnvironmentVariables, StdGen) Maybe EndlessLevel 
    genNewLevel link = do
      -- Advance the environment
      modify $ uncurry $ runRand . nextEnvironment

      -- Set the evs correctly
      mapM_ (modify . set (_1 . specialArea)) link

      -- Make the level
      (lvl, newGen) <- uncurry (runRand . runReaderT generateLevel) <$> get
      modify $ set _2 newGen
      return lvl
  gen <- getStdGen
  runDisplay vty $ handleDisplay $ PlayState
    { _nextLevel    = genNewLevel
    , _curLevelGen  = (startEVs, gen)
    , _save         = initWorld
    , _currentWorld = initWorld
    , _playUIState  = WalkMode
    }

runBuilder :: IO ()
runBuilder = do
  vtyCfg <- standardIOConfig
  vty    <- mkVty vtyCfg
  runDisplay vty $ handleDisplay $ BuildState emptyLevel MovingPlayer startSettings

-- TODO Could we use HSpec to test this?
runRenderTest :: IO ()
runRenderTest = do
  vty <- standardIOConfig >>= mkVty
  screenSize <- genericDisplayBounds $ outputIface vty
  lvlContents <- loadLevel "level1.dat" :: IO (Either String PuzzleLevel)
  case lvlContents of
    -- Print errors if found
    (Left errString) -> putStrLn errString
    -- If no errors ...
    (Right lvl) -> do
      -- Render the level in walk mode
      update vty $ render screenSize
        ( WalkMode :: UIState a ()
        , buildWorld lvl startPlayer startSettings
        )
      -- Eat an input (so the render is visible for more than zero time)
      void $ nextEvent vty
  -- End session
  shutdown vty
