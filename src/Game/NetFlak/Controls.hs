module Game.NetFlak.Controls
  ( getControl
  ) where

import Lens.Simple

import Game.NetFlak.Types.Control
import Game.NetFlak.Types.Settings
import Game.NetFlak.Types.World

import Graphics.Vty

getControl :: Settings -> (Key, [Modifier]) -> Control
getControl settings event
  | (KChar c, []) <- event
  , elem c $ view hotkeys settings
    = UseHotkey c
  | otherwise 
    = case lookup event (view userControls settings) of
      Just x  -> x
      Nothing -> NoControl
