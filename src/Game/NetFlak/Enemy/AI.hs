module Game.NetFlak.Enemy.AI (getNextAction, getEnemyActions, moveTowards, bestAStarMove) where

import Data.Graph.AStar
import Data.HashSet (fromList)
import Data.Maybe

import Game.NetFlak.Enemy
import Game.NetFlak.Level
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyAction
import Game.NetFlak.Types.EnemyMemory
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Vector
import Game.NetFlak.Util

import Lens.Simple

getNextAction :: Level a -> [ Vector ] -> (Vector, Enemy) -> EnemyAction
getNextAction level takenLocations (enemyLoc, enemy)
  -- If the enemy is out of bounds they die
  | not $ inBounds level enemyLoc = Death InWall
  -- Statues on the player die
  | isJust $ view statueHealth enemy
  , enemyLoc == playerLoc = Death Trampled
  -- Other statues do nothing
  | isJust $ view statueHealth enemy = NoAction
  -- If an attack can be made an the current location is free make the attack
  | manhattan playerLoc enemyLoc <= 1
  , elem enemyLoc goodActions
  , view ai enemy == Simple
    = AttackAction (view enemyDamage enemy) (view attackEffects enemy)
  -- If there are no goodChoices the enemy is trampled
  | [] <- goodActions = Death Trampled
  -- If we are able to grow and our current spot is open then grow
  | elem enemyLoc goodActions
  , view enemyGrowth enemy /= NoChange
    = GrowthAction $ case view enemyGrowth enemy of
      Geometric n -> n * (view enemyHealth enemy)
      Linear    n -> n + (view enemyHealth enemy)
  -- If our best choice is not occupied take it.
  | (bestAction : _) <- goodActions = MoveAction bestAction
  where 
    playerLoc = view playerLocation level
    traversableCell cell = inBounds level cell && cell `notElem` takenLocations
    naiveActions = case view ai enemy of
      Simple   -> maybeToList (bestAStarMove traversableCell enemyLoc playerLoc) ++ moveTowards (view memories enemy) playerLoc enemyLoc
      Inactive -> dontMove enemyLoc
    goodActions =
      -- Don't move out of bounds or where another enemy already is
      filter traversableCell $
        -- Don't move where the player is
        filter (/= view playerLocation level) $
          naiveActions

getEnemyActions :: Level a -> [ EnemyAction ]
getEnemyActions lvl = go (fst <$> statues) (view enemies lvl) lvl
  where
    statues = fst $ separateStatues $ view enemies lvl
    go :: [ Vector ] -> [ (Vector, Enemy) ] -> Level a -> [ EnemyAction ]
    go takenLocations [] lvl = []
    go takenLocations (nextEnemy : restEnemies) lvl
      = let
          nextAction   = getNextAction lvl takenLocations nextEnemy
          nextLocation = fst $ enemyAct nextAction nextEnemy
        in
          nextAction : go (nextLocation : takenLocations) restEnemies lvl

-- Movement only if they have to
dontMove :: Vector -> [Vector]
dontMove curr = map (curr +) [(0, 0), (1, 0), (0, 1), (-1, 0), (0, -1)]

bestAStarMove :: (Vector -> Bool) -> Vector -> Vector -> Maybe Vector
bestAStarMove traversable start dest = path >>= listToMaybe
  where
    path = aStar graph (const $ const 1) (manhattan dest) (== dest) start
    graph vert = fromList $ filter traversable $ fmap (+ vert) [ (0, 1), (1, 0), (0, -1), (-1, 0) ]

-- Gives all movements the second vector can make sorted by how helpful they are at reaching the goal.
moveTowards :: EnemyMemory -> Vector -> Vector -> [Vector]
moveTowards em dest curr =
  [ loc
  | loc <- availableSpaces
  , manhattan dest loc < manhattan dest curr
  ] ++
    [curr] ++
      availableSpaces
  where
    verticalMoves   = [(1, 0), (-1, 0)]
    horizontalMoves = [(0, 1), (0, -1)]
    availableSpaces = (curr +) <$> case em of
      Vertical   -> verticalMoves ++ horizontalMoves -- If the last move was vertical prefer vertical
      Horizontal -> horizontalMoves ++ verticalMoves -- If the last move was horiztonal prefer horizontal
      NoMovement -> verticalMoves ++ horizontalMoves -- If there was no movement made prefer verical
