{-# OPTIONS_GHC -fno-warn-overlapping-patterns #-}
module Game.NetFlak.World where

import Prelude hiding
  ( lookup
  )

import Lens.Simple

import Data.List hiding 
  ( delete
  , lookup
  )
import Data.Map.Strict 
  ( insertWith
  , delete
  , update
  , toList
  , lookup
  )

import Game.NetFlak.Color
import Game.NetFlak.Enemy
import Game.NetFlak.Items
import Game.NetFlak.Logs
import Game.NetFlak.RichString
import Game.NetFlak.Spell
import Game.NetFlak.Util

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.Inventory
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.LogItem
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.RichString
import Game.NetFlak.Types.Settings
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.World
import Game.NetFlak.Types.Vector

buildWorld :: Level a -> Player -> Settings -> World a
buildWorld level startingPlayer settings = World {
    _player       = startingPlayer
  , _currentLevel = over enemies (sortBy enemyPriority) level
  , _userSettings = settings
  , _currentSpell = emptySpell
  , _deadEnemies  = []
  , _bloodTracks  = []
  , _richLog      = RichString <$> reverse (view startMessages level)
  , _rawLog       = []
  }

getLimiteds :: [SpellComponent] -> [ComponentStructure]
getLimiteds = (>>= go)
  where
    go (UnlimitedSC _) = []
    go (LimitedSC _ x) = [x]

-- If the player manages to get two copies of the same Limited Spell Component separated then they will be able to pick up that component twice (once for each pile)
pickUp :: ([SpellComponent], [Potion]) -> Inventory -> Inventory
pickUp ([], []) prevInv = prevInv
pickUp ([], (uI : uIs)) prevInv = pickUp ([], uIs) $ over heldPotions (insertWith (+) uI 1) prevInv
pickUp (comp : cs, uIs) prevInv
  | LimitedSC uses contents <- comp
  , elem contents $ getLimiteds $ view heldComponents prevInv
    = pickUp (cs, uIs) $ over heldComponents (combine comp <$>) prevInv
  | otherwise
    = pickUp (cs, uIs) $ over heldComponents (++ [comp]) prevInv
  where
    combine (LimitedSC u1 c1) (LimitedSC u2 c2)
      | c1 == c2 = LimitedSC (u1 + u2) c2
    combine _ comp2 = comp2

performPickUp :: World a -> World a
performPickUp world = 
  over (player . playerInv) (pickUp (snd <$> pickedComps, snd <$> pickedUIs)) $
    set (currentLevel . floorComponents) leftComps $
      set (currentLevel . floorPotions) leftUIs $
        over rawLog (++ newLogs) $
          world
  where
    -- Pickup any items being stood on
    atPlayer = partition ((/= view (currentLevel . playerLocation) world) . fst) 
    (leftComps, pickedComps) = atPlayer $ view (currentLevel . floorComponents) world
    (leftUIs, pickedUIs)     = atPlayer $ view (currentLevel . floorPotions) world

    -- Fill new logs
    newLogs = [ Pickup (itemColor uI) (itemName uI) | (_, uI) <- pickedUIs] ++
              [ Pickup (itemColor sC) (itemName sC) | (_, sC) <- pickedComps]

logPotion :: Potion -> World a -> World a
logPotion item world = over rawLog (UsePotion item :) world

applyPotion :: Potion -> World a -> World a
applyPotion item world = case item of
  HealPot     -> over (player . damageTaken) (subtract 1) world
  HPUp        -> over (player . maxHealth)   (+ 1)        world
  TimePot     -> over (player . effectStack) (+ 1)        world
  Noitop      -> over currentSpell (addConstructionStep DoReverse) world
  Mirror      -> over currentSpell (addConstructionStep DoMirror)  world
  GrowthPot   -> over (currentLevel . enemies . mapped . _2 . enemyHealth) (+1)             world
  WitherPot   -> over (currentLevel . enemies . mapped . _2 . enemyHealth) pred             world
  CollatzPot  -> over (currentLevel . enemies . mapped . _2 . enemyHealth) collatz          world
  StrengthPot -> over (currentLevel . enemies . mapped . _2 . enemyDamage) ((max 0) . pred) world
  WeakPot     -> over (currentLevel . enemies . mapped . _2 . enemyDamage) (+1)             world
  KeyPot      -> set (currentLevel . exitDoors . mapped . isOpen) True world
  _           -> over rawLog (BadPotion item :) world
  where
    collatz :: Integer -> Integer
    collatz n
      | mod n 2 == 0 = div n 2
      | otherwise    = 3 * n + 1

useQuickPotion :: World a -> World a
useQuickPotion world = 
  case view (player . playerInv . quickPotion) world of
    Nothing     -> world
    Just potion -> usePotion potion world

usePotion :: Potion -> World a -> World a
usePotion potion world = 
  case lookup potion (view (player . playerInv . heldPotions) world) of
    Nothing -> world -- Should log message?
    Just 0  -> world -- Should log message?
    Just n  -> 
      case canUse potion world of
        Nothing ->
          logPotion potion $
            applyPotion potion $
              flip (over $ player . playerInv . heldPotions) world $
                case n of
                  1 -> delete potion
                  n -> update (Just . subtract 1) potion
        Just log -> over rawLog (log :) world
        
useItemAtIndex :: (Integral i) => i -> World a -> World a
useItemAtIndex index world
  = case maybeIndex (toList $ view (player . playerInv . heldPotions) world) index of
    Nothing          -> world                  -- Should log message?
    Just (potion, _) -> usePotion potion world
