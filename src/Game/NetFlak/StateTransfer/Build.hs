{-# Language MultiParamTypeClasses, TypeSynonymInstances, FlexibleInstances #-}

module Game.NetFlak.StateTransfer.Build (quitEvent, uiState, nextState) where

import Data.List (genericIndex, genericLength)

import Game.NetFlak.Controls
import Game.NetFlak.Level (reachable, putInBounds)
import Game.NetFlak.ProgramMode

import Game.NetFlak.Render.Build

import Game.NetFlak.Types.BuildState
import Game.NetFlak.Types.Control
import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.Vector

import Graphics.Vty

import Lens.Simple

import System.Random

instance Quittable BuildUI where
  quitEvent QuitBuild = True
  quitEvent _         = False

instance ProgramMode BuildState BuildUI PuzzleLevel where
  uiState = buildUIState
  nextState = nextBuildState
  render = renderBuilder
  renderData = building

bindPositive :: Vector -> Vector
bindPositive (a, b) = (max 1 a, max 1 b)

defaultAdding :: BuildUI
defaultAdding = Adding Nothing

addingEnemy :: BuildUI
addingEnemy = Adding $ Just $ Enemy

addingPotion :: BuildUI
addingPotion = Adding $ Just $ Potion Nothing 

potionSelect :: Integer -> BuildUI
potionSelect = Adding . Just . Potion . Just . Select

potionPlace :: Potion -> Vector -> BuildUI
potionPlace pot place = Adding $ Just $ Potion $ Just $ Place pot place

nextBuildState :: RandomGen g => (Key, [Modifier]) -> (BuildState, g) -> (BuildState, g)
nextBuildState event (oldBuildState, gen) = (newBuildState, gen)
  where
    control = getControl (view buildSettings oldBuildState) event
    clampToLevel :: Vector -> Vector
    clampToLevel (a, b) = (max 0 $ min (sizeA - 1) a, max 0 $ min (sizeB - 1) b)
      where
        (sizeA, sizeB) = view (building . levelSize) oldBuildState
    newBuildState = case (view buildUIState oldBuildState) of
      MovingPlayer -> 
        case control of
          -- TODO prompt
          QuitGame  -> set buildUIState QuitBuild oldBuildState
          Menu (-1) -> set buildUIState Removing oldBuildState
          Menu 1    -> set buildUIState ResizeLevel oldBuildState
          Move disp -> over (building . playerLocation) (clampToLevel . (+ disp)) oldBuildState 
          _         -> oldBuildState
      ResizeLevel ->
        case control of
          QuitGame  -> set buildUIState QuitBuild oldBuildState
          Menu (-1) -> set buildUIState MovingPlayer oldBuildState
          Menu 1    -> set buildUIState MovingDoor oldBuildState
          Move disp ->
            over building putInBounds $
              over (building . levelSize) (bindPositive . (+ disp)) oldBuildState
          _         -> oldBuildState
      MovingDoor    ->
        case control of
          QuitGame  -> set buildUIState QuitBuild oldBuildState
          Menu (-1) -> set buildUIState ResizeLevel oldBuildState
          Menu 1    -> set buildUIState defaultAdding oldBuildState
          Move disp ->
            -- TODO no longer works with multiple doors
            if all (reachable (view building oldBuildState)) (((disp +) . view doorPosition) <$> view (building . exitDoors) oldBuildState)
              then over (building . exitDoors . mapped . doorPosition) (+ disp) oldBuildState 
              else oldBuildState
          _         -> oldBuildState
      Adding Nothing ->
        case control of
          QuitGame   -> set buildUIState QuitBuild oldBuildState
          Menu (-1)  -> set buildUIState MovingDoor oldBuildState
          Menu 1     -> set buildUIState Editing oldBuildState
          MenuSelect -> set buildUIState (Adding $ Just SpellComponent) oldBuildState
          _          -> oldBuildState
      Adding (Just SpellComponent) ->
        case control of
          QuitGame  -> set buildUIState QuitBuild oldBuildState
          ExitMode  -> set buildUIState (Adding Nothing) oldBuildState
          Menu (-1) -> set buildUIState addingEnemy oldBuildState
          Menu 1    -> set buildUIState addingPotion oldBuildState
          _ -> oldBuildState
      Adding (Just (Potion Nothing)) ->
        case control of
          QuitGame  -> set buildUIState QuitBuild oldBuildState
          ExitMode  -> set buildUIState (Adding Nothing) oldBuildState
          Menu (-1)  -> set buildUIState (Adding $ Just SpellComponent) oldBuildState
          Menu 1     -> set buildUIState addingEnemy oldBuildState
          MenuSelect -> set buildUIState (potionSelect 0) oldBuildState
          _ -> oldBuildState
      Adding (Just (Potion (Just (Select n)))) ->
        case control of
          QuitGame   -> set buildUIState QuitBuild oldBuildState
          ExitMode   -> set buildUIState addingPotion oldBuildState
          Menu m     -> set buildUIState (potionSelect $ min (genericLength orderedPotions - 1) $ max 0 $ m + n) oldBuildState
          MenuSelect -> set buildUIState (potionPlace (genericIndex orderedPotions n) (0, 0)) oldBuildState
          _ -> oldBuildState
      Adding (Just (Potion (Just (Place pot location)))) ->
        case control of
          QuitGame   -> set buildUIState QuitBuild oldBuildState
          -- TODO Don't reset the potion index
          ExitMode   -> set buildUIState (potionSelect 0) oldBuildState
          Move disp  -> set buildUIState (potionPlace pot (clampToLevel $ location + disp)) oldBuildState
          MenuSelect -> over (building . floorPotions) ((location, pot) :) oldBuildState
          _ -> oldBuildState
      Adding (Just Enemy) -> 
        case control of
          QuitGame  -> set buildUIState QuitBuild oldBuildState
          ExitMode  -> set buildUIState (Adding Nothing) oldBuildState
          Menu (-1) -> set buildUIState addingPotion oldBuildState
          Menu 1    -> set buildUIState (Adding $ Just SpellComponent) oldBuildState
          _ -> oldBuildState
      Editing   ->
        case control of
          QuitGame  -> set buildUIState QuitBuild oldBuildState
          Menu (-1) -> set buildUIState defaultAdding oldBuildState
          Menu 1    -> set buildUIState Removing oldBuildState
          _         -> oldBuildState
      Removing  ->
        case control of 
          QuitGame  -> set buildUIState QuitBuild oldBuildState
          Menu (-1) -> set buildUIState Editing oldBuildState
          Menu 1    -> set buildUIState MovingPlayer oldBuildState
          _         -> oldBuildState
      QuitBuild -> oldBuildState







