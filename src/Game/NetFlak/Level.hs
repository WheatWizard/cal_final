module Game.NetFlak.Level
  ( inside
  , inBounds
  , reachable
  , putInBounds
  , removeFromDoor
  , isWon
  , enemiesInBounds
  ) where

import Data.Maybe (isNothing)

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.LogItem
import Game.NetFlak.Types.Vector

import Lens.Simple

enemiesInBounds :: Level a -> [(Vector, Enemy)]
enemiesInBounds lvl = filter (inBounds lvl . fst) $ view enemies lvl

-- Checks if a Vector is within the bounds of a box
inside :: Vector -> Vector -> Bool
inside (a, b) (a', b')
  = a >= 0 && b >= 0 && a < a' && b < b'

-- Checks if a vector is in the bounds of the world
inBounds :: Level a -> Vector -> Bool
inBounds lvl loc = loc `inside` view levelSize lvl 

-- Like inBounds but also permits things to be adjacent to the level
reachable :: Level a -> Vector -> Bool
reachable lvl (a, b)
  = a >= (-1) && b >= (-1) && a <= levelA && b <= levelB
  where
    (levelA, levelB) = view levelSize lvl

-- Puts the player back in bounds
putInBounds :: Level a -> Level a
putInBounds lvl = set playerLocation (max 0 $ min sizeA locA, max 0 $ min sizeB locB) lvl
  where
    (locA, locB)   = view playerLocation lvl
    (sizeA, sizeB) = view levelSize lvl - (1, 1)

-- Removes the player from the door
removeFromDoor :: Level a -> Level a
removeFromDoor lvl
  | notElem newLoc $ view doorPosition <$> view exitDoors lvl =
    newLvl
  | otherwise =
    case filter (inBounds lvl) ( (+ newLoc) <$> cardinals ) of
      -- if there is an adjacent inbounds cell put the player there
      (a : _) -> set playerLocation a newLvl
      -- if there is not just make sure the player is in bounds
      []      -> newLvl
  where
    newLvl = putInBounds lvl
    newLoc = view playerLocation newLvl
    cardinals = [(0, 1), (1, 0), (0, -1), (-1, 0)]

isWon :: Level a -> Bool
isWon = null . filter (isNothing . view (_2 . statueHealth)) . view enemies
